cmake_minimum_required(VERSION 2.8)

include(cmake/CodeCoverage.cmake)

project(pgstl)

enable_testing()

# Code coverage support
if (CMAKE_CONFIGURATION_TYPES)
    list(APPEND CMAKE_CONFIGURATION_TYPES CodeCoverage)
    list(REMOVE_DUPLICATES CMAKE_CONFIGURATION_TYPES)
    set(CMAKE_CONFIGURATION_TYPES "${CMAKE_CONFIGURATION_TYPES}" CACHE STRING
        "Add the configurations that we need"
        FORCE)
endif()

set(CMAKE_CXX_FLAGS_CODECOVERAGE
    "${CMAKE_CXX_FLAGS} --coverage"
    CACHE STRING "Flags used by the C++ compiler during coverage builds."
    FORCE)

mark_as_advanced(
  CMAKE_CXX_FLAGS_CODECOVERAGE)


add_definitions("-std=c++11")

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include")

set(GTEST_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/googletest/googletest")
set(GMOCK_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/3rdparty/googletest/googlemock")

add_subdirectory(3rdparty)
add_subdirectory(src)
add_subdirectory(test)

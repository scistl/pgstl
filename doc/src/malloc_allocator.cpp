template <typename _Tp>
class malloc_allocator
{
public:
  using value_type      = _Tp;
  /* ... */

public:
  constexpr
  malloc_allocator() = default;

  template <typename _U>
  constexpr
  malloc_allocator(const malloc_allocator<_U> &)
  { }

  ~malloc_allocator() noexcept = default;

public:
  pointer
  allocate(size_type __n, const_pointer = 0)
  {
    /* ... */

    pointer __p =
      static_cast<pointer>(std::malloc(__n * sizeof(value_type)));

    /* ... */

    return __p;
  }

  void
  deallocate(pointer __p, size_type)
  {
    std::free(__p);
  }

  /* ... */
};

/* ... */

template <typename _Tp>
inline
bool
operator ==(const malloc_allocator<_Tp> &, const malloc_allocator<_Tp> &)
{
  return true;
}

template <typename _Tp>
inline
bool
operator !=(const malloc_allocator<_Tp> &, const malloc_allocator<_Tp> &)
{
  return false;
}

template <class _Tp, class _VoidPtr>
struct __forward_list_node;

template <class _NodePtr>
struct __forward_begin_node
{
  typedef _NodePtr pointer;

  pointer __next_;

  __forward_begin_node()
    : __next_(nullptr) {}
};

template <class _Tp, class _VoidPtr>
struct __begin_node_of
{
  typedef __forward_begin_node<
    typename __rebind_pointer<
      _VoidPtr,
      __forward_list_node<_Tp, _VoidPtr> >::type
    > type;
};

template <class _Tp, class _VoidPtr>
struct __forward_list_node
  : public __begin_node_of<_Tp, _VoidPtr>::type
{
  typedef _Tp value_type;

  value_type __value_;
};

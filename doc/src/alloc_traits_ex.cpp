// alokator
min_allocator<T> a;
// zarezerwowanie pamięci dla jednego elementu
std::allocator_trait::pointer ptr =
   std::allocator_trait::allocate(a, 1);
// skonstruowanie T(x, y, z)
std::allocator_trait::construct(a, ptr, x, y, z);
// wywołanie destruktora \textasciitilde T()
std::allocator_trait::destroy(a, ptr);
// zwolnienie pamięci obiektu
std::allocator_trait::deallocate(a, ptr, 1);

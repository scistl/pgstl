namespace std
{
  // Przeciążenia dla zwykłych wskaźników
  template<typename U, typename T>
  inline
  auto
  static_pointer_cast(T* t) ->
    decltype(static_cast<U*>(t))
  {
    return static_cast<U*>(t);
  }

  template<typename U, typename T>
  inline
  auto
  dynamic_pointer_cast(T* t) ->
    decltype(dynamic_cast<U*>(t))
  {
    return dynamic_cast<U*>(t);
  }

  template<typename U, typename T>
  inline
  auto
  const_pointer_cast(T* t) ->
    decltype(const_cast<U*>(t))
  {
    return const_cast<U*>(t);
  }
} // ~namespace std

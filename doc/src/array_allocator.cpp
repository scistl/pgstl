template
<
  typename _Tp,
  typename _Array = array_storage,
  typename _Pointer = _Tp*
>
class array_allocator
{
  template <typename, typename, typename>
  friend class array_allocator;

public:
  using value_type    = _Tp;
  /* ... */

public:
  array_allocator(array_type* array)
    : _M_array(array)
  { }

  template <typename U, typename V, typename W>
  array_allocator(const array_allocator<U, V, W>& that)
    : _M_array(reinterpret_cast<array_type*>(that._M_array))
  { }

  pointer
  allocate(size_type n, const_pointer = 0)
  {
    return static_cast<pointer>(
      reinterpret_cast<_Tp*>(
        _M_array->allocate(n * sizeof(_Tp), alignof(_Tp))));
  }

  void
  deallocate(pointer p, size_type)
  {
    _M_array->deallocate(&(*p));
  }

private:
  array_type* _M_array;
};

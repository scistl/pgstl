template <typename _Tp, typename _Pointer>
struct _node_ptr
{
  using type =
    typename std::pointer_traits<_Pointer>::template
      rebind<_node<_Tp, _Pointer>>;
};

/* ... */
template <typename _Tp, typename _Alloc>
class _list_base
{
  using _alloc_traits_type  =
    std::allocator_traits<_Alloc>;
  using _void_alloc_traits_type =
    typename _alloc_traits_type::template
      rebind_traits<void>;
  using _void_ptr_type =
    typename _void_alloc_traits_type::pointer;

protected:
  using _node_type          =
    _node<_Tp, _void_ptr_type>;
  using _node_alloc_traits_type =
    typename _alloc_traits_type::template
      rebind_traits<_node_type>;
  using _node_alloc_type    =
    typename _node_alloc_traits_type::allocator_type;
  using _node_pointer       =
    typename _node_alloc_traits_type::pointer;
  using _node_const_pointer =
    typename _node_alloc_traits_type::const_pointer;
  using _Tp_alloc_traits_type =
    typename _alloc_traits_type::template
      rebind_traits<_Tp>;
  using _Tp_alloc_type      =
    typename _Tp_alloc_traits_type::allocator_type;
  /** ... **/
};

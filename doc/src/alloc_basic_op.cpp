// przydział pamięci (allocate)
void * addr = ::operator new(sizeof(T));

// konstrukcja obiektu (construct)
T * p = ::new (addr) T(1, 'x', true);

// wywołanie destruktora (destroy)
p->~T();

// zwolnienie pamięci (deallocate)
::operator delete(addr);

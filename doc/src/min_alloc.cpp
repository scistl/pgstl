#include <cstddef>
template <class T>
class min_allocator
{
public:
  using value_type = T;

public:
  min_allocator(/* parametry konstruktora */);

  template <class U>
  min_allocator(const min_allocator<U>& other);

public:
  T*
  allocate(std::size_t n);

  void
  deallocate(T* p, std::size_t n);
};

template <class T, class U>
bool
operator==(const min_allocator<T>&,
           const min_allocator<U>&);

template <class T, class U>
bool
operator!=(const min_allocator<T>&,
           const min_allocator<U>&);

struct _Fwd_list_node_base
{
  _Fwd_list_node_base() = default;

  _Fwd_list_node_base* _M_next = nullptr;

  _Fwd_list_node_base*
  _M_transfer_after(
    _Fwd_list_node_base* __begin,
    _Fwd_list_node_base* __end) noexcept
  {
    _Fwd_list_node_base* __keep = __begin->_M_next;
    if (__end)
      {
        __begin->_M_next = __end->_M_next;
        __end->_M_next = _M_next;
      }
    else
      __begin->_M_next = 0;
    _M_next = __keep;
    return __end;
  }
}

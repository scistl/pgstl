class allocator
{
  /* ... */
  pointer
  address(reference __x) const
  { return std::__addressof(__x); }
};

class fancy_pointer
{
  /* ... */
  static
  fancy_pointer
  pointer_to(reference __x) const
  { return std::__addressof(__x); }
};

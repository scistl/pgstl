{
  // Lista jednokierunkowa z:
  // - alokatorem typu \texttt{std::alocator<int>}
  // - wskaźnikiem typu \texttt{int*}

  pgstl::singly_linked_list<int> list;
}

{
  // Lista jednokierunkowa z:
  // - alokatorem typu \texttt{pgstl::malloc\char`_allocator<int>}
  // - wskaźnikiem typu \texttt{int*}

  using alloc_type = pgstl::malloc_allocator<int>;

  pgstl::singly_linked_list<int,alloc_type> list;
}

{
  // Lista jednokierunkowa z:
  // - alokatorem typu \texttt{pgstl::array\char`_allocator<int>}
  // - wskaźnikiem typu \texttt{int*}

  using alloc_type = pgstl::array_allocator<int>;

  pgstl::array_storage ary;
  pgstl::alloc_type alc(&ary);
  pgstl::singly_linked_list<int,alloc_type> list(alc);
}

{
  // Lista jednokierunkowa z:
  // - alokatorem typu \texttt{pgstl::array\char`_allocator<int>}
  // - wskaźnikiem typu \texttt{pgstl::array\char`_pointer<int>}

  using alloc_type = pgstl::array_allocator<
    int,pgstl::array_storage,
    pgstl::array_pointer<int>>;

  pgstl::array_storage ary;
  pgstl::alloc_type alc(&ary);
  pgstl::singly_linked_list<int,alloc_type> list(alc);
}

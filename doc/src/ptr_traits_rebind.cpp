// jeśli \texttt{\char`_Pointer} jest:
//   a) typu \texttt{T*} to typ \texttt{type} odpowiada typowi \texttt{U*}
//   b) typu \texttt{fancy\char`_pointer<T>} to typ \texttt{type} odpowiada
//      typowi \texttt{fancy\char`_pointer<U>}
using type =
  typename std::pointer_traits<_Pointer>::template
    rebind<U>;

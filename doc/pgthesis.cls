\NeedsTeXFormat{LaTeX2e}  % dla pełnej jasności
\ProvidesClass{pgthesis} % jedyny niezbędny element klasy 
\DeclareOption*{%                               wszystkie opcje
  \PassOptionsToClass{\CurrentOption}{book}} % mają być przekazywane
                                             % do klasy book
\ProcessOptions
\LoadClass[10pt,a4paper,twoside]{book}
\RequirePackage[OT4]{polski}
\RequirePackage[utf8]{inputenc}

\RequirePackage{uarial} % dostarcza font o~kroju Arial
\RequirePackage[overload]{textcase} % dostarcza definicję MakeTextUppercase

\RequirePackage[titles]{tocloft} % własny Spis Treści

\RequirePackage{fancyhdr} % własny format stopki

\RequirePackage{titlesec} % własne nagłówki rozdziałów, sekcji itp.

% własne podpisy (str. 10)
\RequirePackage[format=plain, labelsep=period, font=small, labelfont=bf]{caption}

% własne marginesy (str. 10)
\RequirePackage[a4paper, left=3.5cm, right=2.5cm, top=2.5cm,
bottom=2.5cm]{geometry}

% wcięcie pierwszego wiersza w paragrafie
\RequirePackage{indentfirst} 

\RequirePackage{tabulary}

% ======================================================================
% Styl PG zgodnie z Wytycznymi nr 49/2014
% ======================================================================

% czcionka Arial (str. 10)
\renewcommand{\familydefault}{\sfdefault}

% odstęp między wierszami 1,5 wiersza (str. 10)
\linespread{1.3}

% wcięcie paragrafu (str. 1,25cm) (str. 10)
\setlength{\parindent}{1.25cm}

% styl stopki (str. 10)
\fancyhf{}
\fancyfoot[C]{\small \thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\fancypagestyle{plain}{%
  \fancyhf{}
  \fancyfoot[C]{\small \thepage}
  \renewcommand{\headrulewidth}{0pt}
  \renewcommand{\footrulewidth}{0pt}}

\pagestyle{fancy}

% styl Spisu Treści (str. 8, 9)
\renewcommand\cftchapaftersnum{.}
\renewcommand\cftsecaftersnum{.}
\renewcommand\cftsubsecaftersnum{.}
\renewcommand\cftsubsubsecaftersnum{.}
\renewcommand\cftfigaftersnum{.}
\renewcommand\cfttabaftersnum{.}
\renewcommand\cftchapfont{\normalfont}
\renewcommand{\cftchapleader}{\cftdotfill{\cftsecdotsep}}
\renewcommand\cftchappagefont{\normalfont}
\renewcommand{\cftdotsep}{0.2}
\renewcommand\cftchapdotsep{\cftdot}
\setlength{\cftbeforechapskip}{0pt}
\renewcommand{\chaptername}{}

% Zmiana standardowych tytułów
% tytuł Literatury (str. 9)
\renewcommand\bibname{Wykaz literatury}
% tytuł Spisu rysunków (str. 9)
\renewcommand\listfigurename{Wykaz rysunków}
% tytuł Spisu tabel (str. 9)
\renewcommand\listtablename{Wykaz tabel}

% Zmiana stylu tytułów (str. 10)
\titleformat{\chapter}[hang]
 {\large\bfseries}{\thechapter{}.}{.5em}{}

\titlespacing*{\chapter} {0pt} {12pt} {6pt}

\titleformat{\section}[hang]
 {\normalsize\bfseries\itshape}{\thesection{}.}{.5em}{}

\titlespacing*{\section} {0pt} {12pt} {6pt}

\titleformat{\subsection}[hang]
 {\normalsize\itshape}{\thesubsection{}.}{.5em}{}

\titlespacing*{\subsection} {0pt} {12pt} {6pt}

% Zmiana stylu podpisów (str. 10)
\captionsetup[table]{singlelinecheck=false, justification=justified,
  position=top, aboveskip=6pt, belowskip=0pt}

\captionsetup[figure]{justification=centering,
  position=below, aboveskip=6pt, belowskip=12pt}

\captionsetup[lstlisting]{justification=centering,
  position=below, aboveskip=6pt, belowskip=12pt}

% Rys. a~nie Rysunek
\renewcommand{\figurename}{Rys.}

% Makra

% Nie dodawaj pustej strony po rozdziale
\newcommand{\pgthesissuppressblankpage}{\let\cleardoublepage\clearpage}

\newcommand{\pgthesisuppercase}{\MakeTextUppercase}

\endinput % dla porządku
find_program(LCOV_EXECUTABLE lcov)
find_program(GENHTML_EXECUTABLE genhtml)


function(SETUP_TARGET_FOR_COVERAGE _targetname _testrunner _outputname)

  if (NOT LCOV_EXECUTABLE)
    MESSAGE(FATAL_ERROR "lcov not found! Aborting...")
  endif() # NOT LCOV_EXECUTABLE

  if (NOT GENHTML_EXECUTABLE)
    MESSAGE(FATAL_ERROR "genhtml not found! Aborting...")
  endif() # NOT GENHTML_EXECUTABLE

  # Setup target
  add_custom_target(${_targetname}

    # Cleanup lcov
    ${LCOV_EXECUTABLE} --directory . --zerocounters

    # Run tests
    COMMAND ${_testrunner} ${ARGV3}

    # Capturing lcov counters and generating report
    COMMAND ${LCOV_EXECUTABLE} --directory . --capture --output-file ${_outputname}.info
    COMMAND ${LCOV_EXECUTABLE} --remove ${_outputname}.info 'googletest*/*' '/usr/*' 'test/*'  --output-file ${_outputname}.info.cleaned
    COMMAND ${GENHTML_EXECUTABLE} -o ${_outputname} ${_outputname}.info.cleaned
    COMMAND ${CMAKE_COMMAND} -E remove ${_outputname}.info ${_outputname}.info.cleaned

    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
    COMMENT "Resetting code coverage counters to zero.\nProcessing code coverage counters and generating report."
    )

  # Show info where to find the report
  add_custom_command(TARGET ${_targetname} POST_BUILD
    COMMAND ;
    COMMENT "Open ./${_outputname}/index.html in your browser to view the coverage report."
    )

endfunction() # SETUP_TARGET_FOR_COVERAGE

#ifndef PGSTL_RED_BLACK_TREE_HPP
#define PGSTL_RED_BLACK_TREE_HPP

#include <cstdint>
#include <memory>

#include "utility.hpp"

namespace pgstl
{
  namespace redblacktree
  {
    enum _color : std::uint8_t
      {
        _S_null  = 0,
        _S_black = 1,
        _S_red   = 2
      };

    template <typename _Tp, typename _Pointer>
    class _node;

    template <typename _Pointer>
    class _red_black_tree_node
    {
      using color_type = _color;

    public:
      using pointer = _Pointer;

    public:
      constexpr
      _red_black_tree_node()
        : _M_parent(nullptr)
        , _M_left(nullptr)
        , _M_right(nullptr)
        , _M_color(_S_null)
      { }

      pointer _M_parent;
      pointer _M_left;
      pointer _M_right;
      _color _M_color;
    };

    namespace detail
    {
      template <typename _Tp, typename _Pointer>
      struct _node_ptr
      {
        using type =
          typename std::pointer_traits<_Pointer>::template rebind<_node<_Tp, _Pointer>>;
      };
    } // ~namespace detail

    template <typename _Tp, typename _Pointer>
    class _node
      : public _red_black_tree_node<typename detail::_node_ptr<_Tp, _Pointer>::type>
    {
    public:
      using value_type      = _Tp;

    private:
      using pointer         = value_type*;
      using const_pointer   = const value_type*;
      using reference       = value_type&;
      using const_reference = const value_type&;

    public:
      constexpr
      _node()
        : _red_black_tree_node<typename detail::_node_ptr<_Tp, _Pointer>::type>()
      { }

      pointer
      _M_valptr() noexcept
      {
        return reinterpret_cast<pointer>(std::addressof(_M_storage));
      }

      const_pointer
      _M_valptr() const noexcept
      {
        return reinterpret_cast<const_pointer>(std::addressof(_M_storage));
      }

      reference
      key() noexcept
      {
        return *_M_valptr();
      }

      const_reference
      key() const noexcept
      {
        return *_M_valptr();
      }

      typename std::aligned_storage<sizeof(_Tp)>::type _M_storage;
    };

    template <typename _Tp, typename _Compare, typename _Alloc>
    class _tree_base
    {
      using size_type           = std::size_t;
      using _allocator_type     = _Alloc;
      using _key_compare_type   = _Compare;

      using _alloc_traits_type  = std::allocator_traits<_Alloc>;
      using _void_alloc_traits_type =
        typename _alloc_traits_type::template rebind_traits<void>;
      using _void_ptr_type          = typename _void_alloc_traits_type::pointer;

    protected:
      using _node_type          = _node<_Tp, _void_ptr_type>;
      using _node_alloc_traits_type =
        typename _alloc_traits_type::template rebind_traits<_node_type>;
      using _node_alloc_type    = typename _node_alloc_traits_type::allocator_type;
      using _node_pointer       = typename _node_alloc_traits_type::pointer;
      using _node_const_pointer = typename _node_alloc_traits_type::const_pointer;

      using _Tp_alloc_traits_type =
        typename _alloc_traits_type::template rebind_traits<_Tp>;
      using _Tp_alloc_type      = typename _Tp_alloc_traits_type::allocator_type;

    protected:
      struct _tree_base_impl
        : public _node_alloc_type
      {
        _tree_base_impl(const _key_compare_type& __key_cmp,
                        const _allocator_type& __alloc)
          : _node_alloc_type(__alloc)
          , _M_key_cmp(__key_cmp)
          , _M_root_node(nullptr)
          , _M_node_count(0)
        { };

        static void
        _S_initialize_guard_node()
        {
          _S_nil_node->_M_parent = _S_nil_node;
          _S_nil_node->_M_left = _S_nil_node;
          _S_nil_node->_M_right = _S_nil_node;
          _S_nil_node->_M_color = _color::_S_black;
        }

        _key_compare_type _M_key_cmp;
        _node_pointer     _M_root_node;
        static _node_pointer _S_nil_node;
        size_type         _M_node_count;
      };

      _tree_base_impl _M_impl;

    public:
      _tree_base(const _key_compare_type& __key_cmp,
                 const _allocator_type& __alloc)
        : _M_impl(__key_cmp, __alloc)
      {
        _M_impl._M_root_node = _M_impl._S_nil_node = _M_get_node();
        _tree_base_impl::_S_initialize_guard_node();
      }

    public:
      const _node_alloc_type&
      _M_get_node_allocator() const
      {
        return _M_impl;
      }

      _Tp_alloc_type
      _M_get_Tp_allocator() const
      {
        return _Tp_alloc_type(_M_get_node_allocator());
      }

      _node_pointer
      _M_get_node()
      {
        return _node_alloc_traits_type::allocate(_M_impl, 1);
      }

      void
      _M_put_node(_node_pointer __p)
      {
        _node_alloc_traits_type::deallocate(_M_impl, __p, 1);
      }

      template<typename ... _Args>
      void
      _M_construct_node(_node_pointer __node, _Args&&... __args)
      {
        try
          {
            ::new (pointer_from(__node)) _node_type;
            _Tp_alloc_type __a = _M_get_Tp_allocator();
            _Tp_alloc_traits_type::construct(
                __a, __node->_M_valptr(), std::forward<_Args>(__args)...);
          }
        catch(...)
          {
            _M_drop_node(__node);
            throw;
          }
      }

      template<typename... _Args>
      _node_pointer
      _M_create_node(_Args&&... __args)
      {
        _node_pointer __node = _M_get_node();
        _M_construct_node(__node, std::forward<_Args>(__args)...);
        return __node;
      }

      void
      _M_destroy_node(_node_pointer __node)
      {
        _Tp_alloc_type __a = _M_get_Tp_allocator();
        _Tp_alloc_traits_type::destroy(__a, __node->_M_valptr());
        __node->~_node_type();
      }

      void
      _M_drop_node(_node_pointer __node)
      {
        _M_destroy_node(__node);
        _M_put_node(__node);
      }

      template<typename... _Args>
      _node_pointer
      _M_insert(_Args&&... __args)
      {
        _node_pointer __z = _M_create_node(std::forward<_Args>(__args)...);
        _M_red_black_tree_insert(__z);
        ++_M_impl._M_node_count;
        return __z;
      }

      // TODO(ajedrych) Check return value
      void
      _M_erase(_node_pointer __node)
      {
        _M_red_black_tree_erase(__node);
        _M_drop_node(__node);
        --_M_impl._M_node_count;
      }

    private:
      void
      _M_red_black_tree_insert(_node_pointer __z)
      {
        _node_pointer __y = _M_impl._S_nil_node;
        _node_pointer __x = _M_impl._M_root_node;
        while (__x != _M_impl._S_nil_node)
          {
            __y = __x;
            if (_M_impl._M_key_cmp(__z->key(), __x->key()))
              {
                __x = __x->_M_left;
              }
            else
              {
                __x = __x->_M_right;
              }
          }
        __z->_M_parent = __y;
        if (__y == _M_impl._S_nil_node)
          {
            _M_impl._M_root_node = __z;
          }
        else if (_M_impl._M_key_cmp(__z->key(), __y->key()))
          {
            __y->_M_left = __z;
          }
        else
          {
            __y->_M_right = __z;
          }

        __z->_M_left = _M_impl._S_nil_node;
        __z->_M_right = _M_impl._S_nil_node;
        __z->_M_color = _color::_S_red;

        _M_red_black_tree_insert_fixup(__z);
      }

      void
      _M_red_black_tree_insert_fixup(_node_pointer __z)
      {
        while (__z->_M_parent->_M_color == _color::_S_red)
          {
            if (__z->_M_parent == __z->_M_parent->_M_parent->_M_left)
              {
                _node_pointer __y = __z->_M_parent->_M_parent->_M_right;
                if (__y->_M_color == _color::_S_red)
                  {
                    __z->_M_parent->_M_color = _color::_S_black;
                    __y->_M_color = _color::_S_black;
                    __z->_M_parent->_M_parent->_M_color = _color::_S_red;
                    __z = __z->_M_parent->_M_parent;
                  }
                else
                  {
                    if (__z == __z->_M_parent->_M_right)
                      {
                        __z = __z->_M_parent;
                        _M_left_rotate(__z);
                      }
                    __z->_M_parent->_M_color = _color::_S_black;
                    __z->_M_parent->_M_parent->_M_color = _color::_S_red;
                    _M_right_rotate(__z->_M_parent->_M_parent);
                  }
              }
            else
              {
                _node_pointer __y = __z->_M_parent->_M_parent->_M_left;
                if (__y->_M_color == _color::_S_red)
                  {
                    __z->_M_parent->_M_color = _color::_S_black;
                    __y->_M_color = _color::_S_black;
                    __z->_M_parent->_M_parent->_M_color = _color::_S_red;
                    __z = __z->_M_parent->_M_parent;
                  }
                else
                  {
                    if (__z == __z->_M_parent->_M_left)
                      {
                        __z = __z->_M_parent;
                        _M_right_rotate(__z);
                      }
                    __z->_M_parent->_M_color = _color::_S_black;
                    __z->_M_parent->_M_parent->_M_color = _color::_S_red;
                    _M_left_rotate(__z->_M_parent->_M_parent);
                  }
              }
          }

        _M_impl._M_root_node->_M_color = _color::_S_black;
      }

      void
      _M_red_black_tree_erase(_node_pointer __z)
      {
        _node_pointer __y = __z;
        _node_pointer __x = _M_impl._S_nil_node;
        _color __y_color = __y->_M_color;
        if (__z->_M_left == _M_impl._S_nil_node)
          {
            __x = __z->_M_right;
            _M_red_black_transplant(__z, __z->_M_right);
          }
        else if (__z->_M_right == _M_impl._S_nil_node)
          {
            __x = __z->_M_left;
            _M_red_black_transplant(__z, __z->_M_left);
          }
        else
          {
            __y = _S_get_min(__z->_M_right, _M_impl._S_nil_node);
            __y_color = __y->_M_color;
            __x = __y->_M_right;
            if (__y->_M_parent == __z)
              {
                __x->_M_parent = __y;
              }
            else
              {
                _M_red_black_transplant(__y, __y->_M_right);
                __y->_M_right = __z->_M_right;
                __y->_M_right->_M_parent = __y;
              }
            _M_red_black_transplant(__z, __y);
            __y->_M_left = __z->_M_left;
            __y->_M_left->_M_parent = __y;
            __y->_M_color = __z->_M_color;
          }
        if (__y_color == _color::_S_black)
          {
            _M_red_black_tree_erase_fixup(__x);
          }
      }

      void
      _M_red_black_tree_erase_fixup(_node_pointer __x)
      {
        while (__x != _M_impl._M_root_node && __x->_M_color == _color::_S_black)
          {
            if (__x == __x->_M_parent->_M_left)
              {
                _node_pointer __w = __x->_M_parent->_M_right;
                if (__w->_M_color == _color::_S_red)
                  {
                    __w->_M_color = _color::_S_black;
                    __x->_M_parent->_M_color = _color::_S_red;
                    _M_left_rotate(__x->_M_parent);
                    __w = __x->_M_parent->_M_right;
                  }
                if (__w->_M_left->_M_color == _color::_S_black
                    && __w->_M_right->_M_color == _color::_S_black)
                  {
                    __w->_M_color = _color::_S_red;
                    __x = __x->_M_parent;
                  }
                else
                  {
                    if (__w->_M_right->_M_color == _color::_S_black)
                      {
                        __w->_M_left->_M_color = _color::_S_black;
                        __w->_M_color = _color::_S_red;
                        _M_right_rotate(__w);
                        __w = __x->_M_parent->_M_right;
                      }
                    __w->_M_color = __x->_M_parent->_M_color;
                    __x->_M_parent->_M_color = _color::_S_black;
                    __w->_M_right->_M_color = _color::_S_black;
                    _M_left_rotate(__x->_M_parent);
                    __x = _M_impl._M_root_node;
                  }
              }
            else
              {
                _node_pointer __w = __x->_M_parent->_M_left;
                if (__w->_M_color == _color::_S_red)
                  {
                    __w->_M_color = _color::_S_black;
                    __x->_M_parent->_M_color = _color::_S_red;
                    _M_right_rotate(__x->_M_parent);
                    __w = __x->_M_parent->_M_left;
                  }
                if (__w->_M_right->_M_color == _color::_S_black
                    && __w->_M_left->_M_color == _color::_S_black)
                  {
                    __w->_M_color = _color::_S_red;
                    __x = __x->_M_parent;
                  }
                else
                  {
                    if (__w->_M_left->_M_color == _color::_S_black)
                      {
                        __w->_M_right->_M_color = _color::_S_black;
                        __w->_M_color = _color::_S_red;
                        _M_left_rotate(__w);
                        __w = __x->_M_parent->_M_left;
                      }
                    __w->_M_color = __x->_M_parent->_M_color;
                    __x->_M_parent->_M_color = _color::_S_black;
                    __w->_M_left->_M_color = _color::_S_black;
                    _M_right_rotate(__x->_M_parent);
                    __x = _M_impl._M_root_node;
                  }
              }
          }
      }

      void
      _M_right_rotate(_node_pointer __y)
      {
        _node_pointer __x = __y->_M_left;
        __y->_M_left = __x->_M_right;
        if (__x->_M_right != _M_impl._S_nil_node)
          {
            __x->_M_right->_M_parent = __y;
          }
        __x->_M_parent = __y->_M_parent;
        if (__y->_M_parent == _M_impl._S_nil_node)
          {
            _M_impl._M_root_node = __x;
          }
        else if (__y == __y->_M_parent->_M_right)
          {
            __y->_M_parent->_M_right = __x;
          }
        else
          {
            __y->_M_parent->_M_left = __x;
          }
        __x->_M_right = __y;
        __y->_M_parent = __x;
      }

      void
      _M_left_rotate(_node_pointer __x)
      {
        _node_pointer __y = __x->_M_right;
        __x->_M_right = __y->_M_left;
        if (__y->_M_left != _M_impl._S_nil_node)
          {
            __y->_M_left->_M_parent = __x;
          }
        __y->_M_parent = __x->_M_parent;
        if (__x->_M_parent == _M_impl._S_nil_node)
          {
            _M_impl._M_root_node = __y;
          }
        else if (__x == __x->_M_parent->_M_left)
          {
            __x->_M_parent->_M_left = __y;
          }
        else
          {
            __x->_M_parent->_M_right = __y;
          }
        __y->_M_left = __x;
        __x->_M_parent = __y;
      }

      void
      _M_red_black_transplant(_node_pointer __u, _node_pointer __v)
      {
        if (__u->_M_parent == _M_impl._S_nil_node)
          {
            _M_impl._M_root_node = __v;
          }
        else if (__u == __u->_M_parent->_M_left)
          {
            __u->_M_parent->_M_left = __v;
          }
        else
          {
            __u->_M_parent->_M_right = __v;
          }

        __v->_M_parent = __u->_M_parent;
      }
    };

    template <typename _Pointer>
    inline _Pointer
    _S_get_min(_Pointer __x, _Pointer __nil)
    {
      while (__x->_M_left != __nil)
        {
          __x = __x->_M_left;
        }

      return __x;
    }

    template <typename _Pointer>
    inline _Pointer
    _S_get_max(_Pointer __x, _Pointer __nil)
    {
      while (__x->_M_right != __nil)
        {
          __x = __x->_M_right;
        }

      return __x;
    }

    template <typename _Pointer>
    inline _Pointer
    _S_get_next(_Pointer __x, _Pointer __nil)
    {
      if (__x->_M_right != __nil)
        {
          return _S_get_min(__x->_M_right, __nil);
        }
      _Pointer __y = __x->_M_parent;
      while (__y != __nil && __x == __y->_M_right)
        {
          __x = __y;
          __y = __y->_M_parent;
        }
      return __y;
    }

    template <typename _Pointer>
    inline _Pointer
    _S_get_prev(_Pointer __x, _Pointer __nil)
    {
      if (__x->_M_left != __nil)
        {
          return _S_get_max(__x->_M_left, __nil);
        }
      _Pointer __y = __x->_M_parent;
      while (__y != __nil && __x == __y->_M_left)
        {
          __x = __y;
          __y = __y->_M_parent;
        }
      return __y;
    }

    template <typename _Tp, typename _Compare, typename _Alloc>
    typename _tree_base<_Tp,_Compare,_Alloc>::_node_pointer
      _tree_base<_Tp,_Compare,_Alloc>::_tree_base_impl::_S_nil_node = nullptr;
  } // ~namespace redblacktree

  template <typename _Pointer>
  class _red_black_tree_iterator
  {
    using _node_pointer = _Pointer;
    using _self_type = _red_black_tree_iterator<_Pointer>;
  public:
    using node_type  =
      typename std::pointer_traits<_node_pointer>::element_type;

  public:
    _red_black_tree_iterator(_node_pointer __nil)
      : _M_node_ptr(__nil)
      , _M_nil_ptr(__nil)
    { }

    _red_black_tree_iterator(_node_pointer __ptr, _node_pointer __nil)
      : _M_node_ptr(__ptr)
      , _M_nil_ptr(__nil)
    { }

    node_type&
    operator*()
    {
      return *_M_node_ptr;
    }

    _node_pointer
    operator->() const
    {
      return _M_node_ptr;
    }

    _self_type&
    operator++()
    {
      _M_node_ptr = _S_get_next(_M_node_ptr, _M_nil_ptr);
      return *this;
    }

    _self_type
    operator++(int)
    {
      _self_type __t(*this);
      ++(*this);
      return __t;
    }

    bool
    operator !=(const _self_type& that)
    {
      return _M_node_ptr != that._M_node_ptr;
    }

  private:
    _node_pointer _M_node_ptr;
    _node_pointer _M_nil_ptr;
  };

  template <typename _Tp, typename _Compare, typename _Alloc = std::allocator<_Tp>>
  class red_black_tree
    : protected redblacktree::_tree_base<_Tp, _Compare, _Alloc>
  {
    using _base_type      = redblacktree::_tree_base<_Tp, _Compare, _Alloc>;
    using _allocator_type = _Alloc;
    using _key_compare_type = _Compare;

  public:
    using size_type       = std::size_t;
    using iterator        =
      _red_black_tree_iterator<typename _base_type::_node_pointer>;


  public:
    red_black_tree()
      : _base_type()
    { }

    red_black_tree(const _allocator_type& __alloc)
      : _base_type(_key_compare_type(), __alloc)
    { }

  public:
    size_type
    size() const
    {
      return this->_M_impl._M_node_count;
    }

    bool
    empty() const
    {
      return this->_M_impl._M_root_node == this->_M_impl._S_nil_node;
    }

    iterator
    insert(_Tp&& key)
    {
      return iterator(this->_M_insert(key), this->_M_impl._S_nil_node);
    }

    iterator
    insert(const _Tp& key)
    {
      return iterator(this->_M_insert(key), this->_M_impl._S_nil_node);
    }

    void
    erase(const iterator& it)
    {
      this->_M_erase(it.operator->());
    }

    iterator
    begin()
    {
      return iterator(_S_get_min(this->_M_impl._M_root_node,
                                 this->_M_impl._S_nil_node),
                      this->_M_impl._S_nil_node);
    }

    iterator
    end()
    {
      return iterator(this->_M_impl._S_nil_node);
    }
  };
} // ~namespace pgstl

#endif // PGSTL_RED_BLACK_TREE_HPP

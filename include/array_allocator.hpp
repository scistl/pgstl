#ifndef PGSTL_ARRAY_ALLOCATOR
#define PGSTL_ARRAY_ALLOCATOR

#include <memory>
#include <type_traits>

namespace pgstl
{
  class array_storage
  {
  public:
    using size_type     = std::size_t;
    using pointer       = void*;
    using const_pointer = const void*;

  public:
    array_storage()
      : _M_storage{}
      , _M_offset(0)
    {
      _S_data = &_M_storage[0];
    }

    ~array_storage() = default;

  public:
    pointer
    allocate(size_type __n, size_type __align)
    {
      _M_offset = (_M_offset + __align - 1) / __align * __align;
      //FIXME(ajedrych) Check allocated memory and expand if necessary
      pointer __pos = _M_storage + _M_offset;
      _M_offset += __n;
      return __pos;
    }

    void
    deallocate(pointer)
    { }

    void
    reset()
    {
      _M_offset = 0;
    }

  public:
    static pointer _S_data;

  private:
    std::uint8_t _M_storage[1024 * 1024];
    size_type    _M_offset;
  };

  template <typename _Pointer>
  class array_pointer_base
  {
    template <typename>
    friend class array_pointer_base;

    using size_type       = std::size_t;

  public:
    array_pointer_base()
      : _M_array(nullptr)
      , _M_idx(0)
    { }

    array_pointer_base(std::nullptr_t)
      : _M_array(nullptr)
      , _M_idx(0)
    { }

    template <typename _PointerThat>
    array_pointer_base(const array_pointer_base<_PointerThat>& __that)
      : _M_array(const_cast<_Pointer>(__that._M_array))
      , _M_idx(__that._M_idx)
    { }

  protected:
    array_pointer_base(_Pointer __pos, size_t __idx)
      : _M_array(__pos)
      , _M_idx(__idx)
    { }


  protected:
    _Pointer  _M_array;
    size_type _M_idx;
  };


  template <typename _Tp>
  class array_pointer
    : public array_pointer_base<_Tp*>
  {
    template <typename R, typename U>
    friend
    bool
    operator==(const array_pointer<R>&, const array_pointer<U>&);

  public:
    using element_type    = _Tp;
    using reference       = element_type&;
    using pointer         = element_type*;

  public:
    array_pointer()
      : array_pointer_base<_Tp*>()
    { }

    array_pointer(std::nullptr_t)
      : array_pointer_base<_Tp*>(nullptr)
    { }

    explicit
    array_pointer(_Tp* __address)
      : array_pointer_base<_Tp*>(static_cast<_Tp*>(array_storage::_S_data),
                               __address - static_cast<_Tp*>(array_storage::_S_data))
    { }

    template <typename _Tp1>
    array_pointer(const array_pointer<_Tp1>& __that)
      : array_pointer_base<_Tp*>(__that)
    { }

  private:
    array_pointer(_Tp* __pos, size_t __idx)
      : array_pointer_base<_Tp*>(__pos, __idx)
    { }

  public:
    static
    array_pointer<_Tp>
    pointer_to(reference)
    {
      return array_pointer<_Tp>(static_cast<_Tp*>(array_storage::_S_data), 0);
    }

    reference
    operator*() const noexcept
    {
      return this->_M_array[this->_M_idx];
    }

    pointer
    operator->() const noexcept
    {
      pointer __result = nullptr;

      if (this->_M_array != nullptr)
        {
          __result = std::addressof(this->_M_array[this->_M_idx]);
        }

      return __result;
    }

    explicit
    operator bool() const
    {
      return this->_M_array != nullptr && this->_M_idx != 0;
    }
  };

  template <>
  class array_pointer<void>
    : public array_pointer_base<void*>
  {
  public:
    using pointer         = void*;
  };


  template <typename T, typename U>
  bool
  operator==(const array_pointer<T>& __lhs, const array_pointer<U>& __rhs)
  {
    return __lhs._M_array == __rhs._M_array
      && __lhs._M_idx == __rhs._M_idx;
  }

  template <typename T, typename U>
  bool
  operator!=(const array_pointer<T>& __lhs, const array_pointer<U>& __rhs)
  {
    return !(__lhs == __rhs);
  }

  template <typename T>
  bool
  operator==(const array_pointer<T>& __lhs, std::nullptr_t __np)
  {
    array_pointer<T> null(__np);
    return __lhs == null;
  }

  template <typename T>
  bool
  operator==(std::nullptr_t __np, const array_pointer<T>& __rhs)
  {
    array_pointer<T> null(__np);
    return null == __rhs;
  }

  template <typename T>
  bool
  operator!=(const array_pointer<T>& lhs, std::nullptr_t n)
  {
    array_pointer<T> null(n);
    return lhs != null;
  }

  template <typename T>
  bool
  operator!=(std::nullptr_t __np, const array_pointer<T>& __rhs)
  {
    array_pointer<T> null(__np);
    return null != __rhs;
  }


  template <typename _Tp>
  class array_fixed_pointer
  {
    template <typename R, typename U>
    friend
    bool
    operator==(const array_fixed_pointer<R>&, const array_fixed_pointer<U>&);

    struct _impl
    {
      using size_type       = std::size_t;

    public:
      _impl()
        : _M_idx(0)
      { }

      _impl(const _impl& __that) = default;

      explicit _impl(size_t __idx)
        : _M_idx(__idx)
      { }

    public:
      size_type _M_idx;
    };


  public:
    using element_type    = _Tp;
    using reference       = element_type&;
    using pointer         = element_type*;

  public:
    array_fixed_pointer()
      : _M_impl()
    { }

    array_fixed_pointer(std::nullptr_t)
      : _M_impl()
    { }

    explicit
    array_fixed_pointer(_Tp* __address)
      : _M_impl(__address - static_cast<_Tp*>(array_storage::_S_data))
    { }

    // TODO(ajedrych) We should rather use _M_impl(__that._M_impl)
    template <typename _Tp1>
    array_fixed_pointer(const array_fixed_pointer<_Tp1>& __that)
      : _M_impl(__that._M_impl._M_idx)
    { }

  private:
    explicit array_fixed_pointer(size_t __idx)
      : _M_impl(__idx)
    { }

  public:
    static
    array_fixed_pointer<_Tp>
    pointer_to(reference)
    {
      return array_fixed_pointer<_Tp>(0);
    }

    reference
    operator*() const noexcept
    {
      return static_cast<_Tp*>(array_storage::_S_data)[this->_M_impl._M_idx];
    }

    pointer
    operator->() const noexcept
    {
      pointer __result = nullptr;

      if (array_storage::_S_data != nullptr)
        {
          __result = std::addressof(
            static_cast<_Tp*>(array_storage::_S_data)[this->_M_impl._M_idx]);
        }

      return __result;
    }

    explicit
    operator bool() const
    {
      return array_storage::_S_data != nullptr && this->_M_impl._M_idx != 0;
    }

    _impl _M_impl;
  };

  template <>
  class array_fixed_pointer<void>
  {
  public:
    using pointer         = void*;
  };


  template <typename T, typename U>
  bool
  operator==(const array_fixed_pointer<T>& __lhs, const array_fixed_pointer<U>& __rhs)
  {
    return __lhs._M_impl._M_idx == __rhs._M_impl._M_idx;
  }

  template <typename T, typename U>
  bool
  operator!=(const array_fixed_pointer<T>& __lhs, const array_fixed_pointer<U>& __rhs)
  {
    return !(__lhs == __rhs);
  }

  template <typename T>
  bool
  operator==(const array_fixed_pointer<T>& __lhs, std::nullptr_t __np)
  {
    array_fixed_pointer<T> null(__np);
    return __lhs == null;
  }

  template <typename T>
  bool
  operator==(std::nullptr_t __np, const array_fixed_pointer<T>& __rhs)
  {
    array_fixed_pointer<T> null(__np);
    return null == __rhs;
  }

  template <typename T>
  bool
  operator!=(const array_fixed_pointer<T>& lhs, std::nullptr_t n)
  {
    array_fixed_pointer<T> null(n);
    return lhs != null;
  }

  template <typename T>
  bool
  operator!=(std::nullptr_t __np, const array_fixed_pointer<T>& __rhs)
  {
    array_fixed_pointer<T> null(__np);
    return null != __rhs;
  }

  template <typename _Tp, typename _Array = array_storage, typename _Pointer = _Tp*>
  class array_allocator
  {
    template <typename, typename, typename>
    friend class array_allocator;

  public:
    using value_type    = _Tp;
    using array_type    = _Array;

    // NOTICE(ajedrych) This is needed because alloc_trait<T>::rebind rebinds
    //                  only first parameter
    using pointer       =
      typename std::pointer_traits<_Pointer>::template rebind<_Tp>;
    using const_pointer =
      typename std::pointer_traits<_Pointer>::template rebind<const _Tp>;

    using size_type     = std::size_t;

  public:
    array_allocator(array_type* array)
      : _M_array(array)
    { }

    template <typename U, typename V, typename W>
    array_allocator(const array_allocator<U, V, W>& that)
      : _M_array(reinterpret_cast<array_type*>(that._M_array))
    { }

    pointer
    allocate(size_type n, const_pointer = 0)
    {
      // FIXME(ajedrych)
      return static_cast<pointer>(
        reinterpret_cast<_Tp*>(_M_array->allocate(n * sizeof(_Tp), alignof(_Tp))));
    }

    void
    deallocate(pointer p, size_type)
    {
      _M_array->deallocate(&(*p));
    }

  private:
    array_type* _M_array;
  };

  template <typename _Array, typename _Pointer>
  class array_allocator<void, _Array, _Pointer>
  {
  public:
    using value_type    = void;
    using pointer       =
      typename std::pointer_traits<_Pointer>::template rebind<value_type>;
    using const_pointer =
      typename std::pointer_traits<_Pointer>::template rebind<const value_type>;
  };

} // ~namespace pgstl

namespace std
{
  template <class _T, class _U, class _V>
  basic_ostream<_U, _V>& operator<<(basic_ostream<_U, _V>& __os,
                                  const ::pgstl::array_pointer<_T>& __ptr)
  {
    return __os << __ptr.operator->();
  }

  template<typename _T, typename _U>
  inline
  ::pgstl::array_pointer<_T>
  const_pointer_cast(const ::pgstl::array_pointer<_U>& __t)
  {
    return ::pgstl::array_pointer<_T>(__t);
  }

  template <class _T, class _U, class _V>
  basic_ostream<_U, _V>& operator<<(basic_ostream<_U, _V>& __os,
                                  const ::pgstl::array_fixed_pointer<_T>& __ptr)
  {
    return __os << __ptr.operator->();
  }

  template<typename _T, typename _U>
  inline
  ::pgstl::array_fixed_pointer<_T>
  const_pointer_cast(const ::pgstl::array_fixed_pointer<_U>& __t)
  {
    return ::pgstl::array_fixed_pointer<_T>(__t);
  }
} // ~namespace std

#endif // PGSTL_ARRAY_ALLOCATOR

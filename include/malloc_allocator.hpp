#ifndef PGSTL_MALLOC_ALLOCATOR_HPP
#define PGSTL_MALLOC_ALLOCATOR_HPP


#include <cstdlib>
#include <limits>
#include <new>


namespace pgstl
{
  /**
   *  @brief An allocator based on the malloc and free.
   *
   *  @note This allocator uses a default (from std::alloc_traits) construct and
   *  destroy.
   */
  template <typename _Tp>
  class malloc_allocator
  {
  public:
    using value_type      = _Tp;
    using pointer         = value_type*;
    using const_pointer   = const value_type*;
    using size_type       = std::size_t;
    using difference_type = std::ptrdiff_t;

  public:
    constexpr
    malloc_allocator() = default;

    template <typename _U>
    constexpr
    malloc_allocator(const malloc_allocator<_U> &)
    { }

    ~malloc_allocator() noexcept = default;

  public:
    pointer
    allocate(size_type __n, const_pointer = 0)
    {
      if (__n > max_size())
        {
          throw std::bad_alloc();
        }

      pointer __p =
        static_cast<pointer>(std::malloc(__n * sizeof(value_type)));

      if (!__p)
        {
          throw std::bad_alloc();
        }

      return __p;
    }

    void
    deallocate(pointer __p, size_type)
    {
      std::free(__p);
    }

    constexpr
    size_type
    max_size()
    {
      return std::numeric_limits<size_type>::max() / sizeof(_Tp);
    }
  };

  template<>
  struct malloc_allocator<void>
  {
    using value_type    = void;
    using pointer       = value_type*;
    using const_pointer = const value_type*;
    using size_type     = std::size_t;

  public:
    pointer
    allocate(size_type __n, const_pointer = 0)
    {
      if (__n > max_size())
        {
          throw std::bad_alloc();
        }

      pointer __p = static_cast<pointer>(std::malloc(__n));

      if (!__p)
        {
          throw std::bad_alloc();
        }

      return __p;
    }

    void
    deallocate(pointer __p, size_type)
    {
      std::free(__p);
    }

    constexpr
    size_type
    max_size()
    {
      return std::numeric_limits<size_type>::max();
    }
  };

  template <typename _Tp>
  inline
  bool
  operator ==(const malloc_allocator<_Tp> &, const malloc_allocator<_Tp> &)
  {
    return true;
  }

  template <typename _Tp>
  inline
  bool
  operator !=(const malloc_allocator<_Tp> &, const malloc_allocator<_Tp> &)
  {
    return false;
  }
} // ~namespace pgstl

#endif // ~PGSTL_MALLOC_ALLOCATOR_HPP

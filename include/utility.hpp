#ifndef PGSTL_UTILITY_HPP
#define PGSTL_UTILITY_HPP

namespace std
{
  // from http://wg21.cmeerw.net/lwg/issue1289

  //Overloads for raw pointers
  template<typename U, typename T>
  inline
  auto
  static_pointer_cast(T* t) -> decltype(static_cast<U*>(t))
  {
    return static_cast<U*>(t);
  }

  template<typename U, typename T>
  inline
  auto
  dynamic_pointer_cast(T* t) -> decltype(dynamic_cast<U*>(t))
  {
    return dynamic_cast<U*>(t);
  }

  template<typename U, typename T>
  inline
  auto
  const_pointer_cast(T* t) -> decltype(const_cast<U*>(t))
  {
    return const_cast<U*>(t);
  }
} // ~namespace std

namespace pgstl
{
  template <typename _Tp>
  auto pointer_from(_Tp __ptr) -> decltype(std::addressof(*__ptr))
  {
    return !__ptr ? nullptr : std::addressof(*__ptr);
  }

  template <typename _Tp>
  _Tp* pointer_from(_Tp* __ptr)
  {
    return __ptr;
  }
} // namespace pgstl

#endif // ~PGSTL_UTILITY_HPP

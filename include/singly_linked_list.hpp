#ifndef PGSTL_SINGLY_LINKED_LIST_HPP
#define PGSTL_SINGLY_LINKED_LIST_HPP

#include <memory>
#include <ostream>
#include <utility>

#include "utility.hpp"

namespace pgstl
{
  namespace singlylinkedlist
  {

    template <typename _Tp, typename _Pointer>
    class _node;


    template <typename _Pointer>
    class _singly_linked_node
    {
    public:
      using pointer = _Pointer;

      constexpr
      _singly_linked_node()
        : _M_next(nullptr)
      { }

      pointer _M_next;
    };

    namespace detail
    {

      template <typename _Tp, typename _Pointer>
      struct _node_ptr
      {
        using type =
          typename std::pointer_traits<_Pointer>::template rebind<_node<_Tp, _Pointer>>;
      };

    } // ~namespace detail


    template <typename _Tp, typename _Pointer>
    class _node
      : public _singly_linked_node<typename detail::_node_ptr<_Tp, _Pointer>::type>
    {
    public:
      using value_type      = _Tp;

    private:
      using pointer         = value_type*;
      using const_pointer   = const value_type*;
      using reference       = value_type&;
      using const_reference = const value_type&;

    public:
      constexpr
      _node() = default;

      pointer
      _M_valptr() noexcept
      {
        return reinterpret_cast<pointer>(std::addressof(_M_storage));
      }

      const_pointer
      _M_valptr() const noexcept
      {
        return reinterpret_cast<const_pointer>(std::addressof(_M_storage));
      }

      reference
      value() noexcept
      {
        return *_M_valptr();
      }

      const_reference
      value() const noexcept
      {
        return *_M_valptr();
      }


      typename std::aligned_storage<sizeof(_Tp)>::type _M_storage;
    };


    template <typename _Tp, typename _Alloc>
    class _list_base
    {
      using _alloc_traits_type  = std::allocator_traits<_Alloc>;
      using _void_alloc_traits_type =
        typename _alloc_traits_type::template rebind_traits<void>;
      using _void_ptr_type          = typename _void_alloc_traits_type::pointer;

    protected:
      using _node_type          = _node<_Tp, _void_ptr_type>;
      using _node_alloc_traits_type =
        typename _alloc_traits_type::template rebind_traits<_node_type>;
      using _node_alloc_type    = typename _node_alloc_traits_type::allocator_type;
      using _node_pointer       = typename _node_alloc_traits_type::pointer;
      using _node_const_pointer = typename _node_alloc_traits_type::const_pointer;

      using _Tp_alloc_traits_type =
        typename _alloc_traits_type::template rebind_traits<_Tp>;
      using _Tp_alloc_type      = typename _Tp_alloc_traits_type::allocator_type;


    protected:
      struct _list_impl
        : public _node_alloc_type
      {
        _node_pointer _M_head;

        constexpr
        _list_impl()
          : _node_alloc_type(), _M_head(nullptr)
        { }

        constexpr
        _list_impl(const _node_alloc_type& _a)
          : _node_alloc_type(_a), _M_head(nullptr)
        { }
      };

      _list_impl _M_impl;

    public:
      _list_base()
        : _M_impl()
      {
        _M_impl._M_head = _M_create_guard_node();
      }

      explicit _list_base(const _Alloc& __alloc)
        : _M_impl(__alloc)
      {
        _M_impl._M_head = _M_create_guard_node();
      }

      ~_list_base()
      {
        _M_clear();
        _M_destroy_guard_node();
      }

      void
      _M_clear()
      {
        _M_erase_after(_M_impl._M_head, nullptr);
      }

      template<typename... _Args>
      _node_pointer
      _M_insert_after(_node_const_pointer __pos, _Args&&... __args)
      {
        _node_pointer __to = std::const_pointer_cast<_node_type>(__pos);
        _node_pointer __new_node = _M_create_node(std::forward<_Args>(__args)...);
        __new_node->_M_next = __to->_M_next;
        __to->_M_next = __new_node;

        return __to->_M_next;
      }

      _node_pointer
      _M_erase_after(_node_pointer __pos, _node_pointer __last)
      {
        _node_pointer __curr = __pos->_M_next;
        while (__curr != __last)
          {
            _node_pointer __temp = __curr;
            __curr = __curr->_M_next;
            _M_drop_node(__temp);
          }
        __pos->_M_next = __last;
        return __last;
      }

      _node_pointer
      _M_erase_after(_node_pointer __pos)
      {
        _node_pointer __temp = __pos->_M_next;
        __pos->_M_next = __temp->_M_next;
        _M_drop_node(__temp);
        return __pos->_M_next;
      }

      const _node_alloc_type&
      _M_get_node_allocator() const
      {
        return _M_impl;
      }

      _Tp_alloc_type
      _M_get_Tp_allocator() const
      {
        return _Tp_alloc_type(_M_get_node_allocator());
      }

      _node_pointer
      _M_get_node()
      {
        return _node_alloc_traits_type::allocate(_M_impl, 1);
      }

      void
      _M_put_node(_node_pointer __p)
      {
        _node_alloc_traits_type::deallocate(_M_impl, __p, 1);
      }

      template<typename ... _Args>
      void
      _M_construct_node(_node_pointer __node, _Args&&... __args)
      {
        try
          {
            ::new (pointer_from(__node)) _node_type;
            _Tp_alloc_type __a = _M_get_Tp_allocator();
            _Tp_alloc_traits_type::construct(
                __a, __node->_M_valptr(), std::forward<_Args>(__args)...);
          }
        catch(...)
          {
            _M_drop_node(__node);
            throw;
          }
      }

      template<typename ... _Args>
      _node_pointer
      _M_create_node(_Args&&... __args)
      {
        _node_pointer __node = _M_get_node();
        _M_construct_node(__node, std::forward<_Args>(__args)...);
        return __node;
      }

      void
      _M_destroy_node(_node_pointer __node)
      {
        _Tp_alloc_type __a = _M_get_Tp_allocator();
        _Tp_alloc_traits_type::destroy(__a, __node->_M_valptr());
        __node->~_node_type();
      }

      void
      _M_drop_node(_node_pointer __node)
      {
        _M_destroy_node(__node);
        _M_put_node(__node);
      }

    private:
      _node_pointer
      _M_create_guard_node()
      {
        _node_pointer __node = _M_get_node();
        ::new (pointer_from(__node)) _node_type;
        return __node;
      }

      void
      _M_destroy_guard_node()
      {
        _M_impl._M_head->~_node_type();
        _M_put_node(_M_impl._M_head);
      }
    };
  } // ~namespace singlylinkedlist


  template<typename _NodePointer>
  class _singly_linked_list_iterator
  {
    using _node_pointer = _NodePointer;

    using _self_type = _singly_linked_list_iterator<_node_pointer>;


  public:
    using iterator_category = std::forward_iterator_tag;
    using node_type  =
      typename std::pointer_traits<_node_pointer>::element_type;

  private:
    _node_pointer _M_node;

  public:
    constexpr
    _singly_linked_list_iterator()
      : _M_node(nullptr)
    { }

    constexpr
    _singly_linked_list_iterator(_node_pointer __x)
      : _M_node(__x)
    { }

    node_type&
    operator*()
    {
      return *_M_node;
    }

    _node_pointer
    operator->() const noexcept
    {
      return _M_node;
    }

    _self_type&
    operator++()
    {
      this->_M_node = this->_M_node->_M_next;
      return *this;
    }

    _self_type
    operator++(int)
    {
      _self_type __t(*this);
      ++(*this);
      return __t;
    }
  };

  template<typename _NodePointer>
  bool
  operator==(const _singly_linked_list_iterator<_NodePointer>& lhs,
             const _singly_linked_list_iterator<_NodePointer>& rhs)
  {
    return lhs.operator->() == rhs.operator->();
  }


  template<typename _NodePointer>
  bool
  operator!=(const _singly_linked_list_iterator<_NodePointer>& lhs,
             const _singly_linked_list_iterator<_NodePointer>& rhs)
  {
    return !(lhs == rhs);
  }


  template<typename _NodeConstPointer>
  class _singly_linked_list_const_iterator
  {
    using _node_const_pointer = _NodeConstPointer;

    using _self_type = _singly_linked_list_const_iterator<_node_const_pointer>;

  public:
    using iterator_category = std::forward_iterator_tag;
    using node_type  =
      typename std::pointer_traits<_node_const_pointer>::element_type;

  private:
    _node_const_pointer _M_node;

  public:
    constexpr
    _singly_linked_list_const_iterator()
      : _M_node(nullptr)
    { }

    explicit
    constexpr
    _singly_linked_list_const_iterator(_node_const_pointer __x)
      : _M_node(__x)
    { }

    constexpr
    _singly_linked_list_const_iterator(
      const _singly_linked_list_iterator<_NodeConstPointer>& __that)
      : _M_node(__that.operator->())
    { }

    node_type&
    operator*()
    {
      return *_M_node;
    }

    _node_const_pointer
    operator->() const noexcept
    {
      return _M_node;
    }

    _self_type&
    operator++()
    {
      this->_M_node = this->_M_node->_M_next;
      return *this;
    }

    _self_type
    operator++(int)
    {
      _self_type __t(*this);
      ++(*this);
      return __t;
    }
  };

  template<typename _NodeConstPointer>
  bool
  operator==(const _singly_linked_list_const_iterator<_NodeConstPointer>& lhs,
             const _singly_linked_list_const_iterator<_NodeConstPointer>& rhs)
  {
    return lhs.operator->() == rhs.operator->();
  }


  template<typename _NodeConstPointer>
  bool
  operator!=(const _singly_linked_list_const_iterator<_NodeConstPointer>& lhs,
             const _singly_linked_list_const_iterator<_NodeConstPointer>& rhs)
  {
    return !(lhs == rhs);
  }


  template <typename _Tp, typename _Alloc = std::allocator<_Tp>>
    class singly_linked_list
    : protected singlylinkedlist::_list_base<_Tp, _Alloc>
    {
      using _base_type      = singlylinkedlist::_list_base<_Tp, _Alloc>;
      using _node_alloc_traits_type =
        typename _base_type::_node_alloc_traits_type;

      using _Tp_alloc_traits_type = typename _base_type::_Tp_alloc_traits_type;

    public:
      using node_type       = typename _node_alloc_traits_type::value_type;
      using node_reference  = node_type&;
      using node_pointer    = typename _node_alloc_traits_type::pointer;
      using node_const_pointer = typename _node_alloc_traits_type::const_pointer;
      using node_pointer_traits_type =
        typename std::pointer_traits<node_pointer>;

      using value_type      = typename _Tp_alloc_traits_type::value_type;
      using reference       = typename _Tp_alloc_traits_type::value_type&;
      using const_reference = const typename _Tp_alloc_traits_type::value_type&;
      using pointer         = typename _Tp_alloc_traits_type::pointer;


      using const_iterator  = _singly_linked_list_const_iterator<node_pointer>;
      using iterator        = _singly_linked_list_iterator<node_pointer>;

    public:
      constexpr
      singly_linked_list()
        : _base_type()
      { }

      constexpr
      singly_linked_list(const _Alloc& __alloc)
        : _base_type(__alloc)
      { }

    public:
      bool
      empty() const noexcept
      {
        return this->_M_impl._M_head->_M_next == nullptr;
      }

      iterator
      insert_after(const_iterator __pos, const _Tp& __val)
      {
        return this->_M_insert_after(__pos.operator->(), __val);
      }

      iterator
      erase_after(const_iterator __pos)
      {
        return this->_M_erase_after(__pos.operator->());
      }

      void
      push_front(const_reference __val)
      {
        this->_M_insert_after(
          std::const_pointer_cast<const node_type>(this->_M_impl._M_head), __val);
      }

      void
      pop_front()
      {
        this->_M_erase_after(this->_M_impl._M_head);
      }

      void
      clear() noexcept
      {
        this->_M_clear();
      };

      node_reference
      head()
      {
        return *(this->_M_impl._M_head->_M_next);
      }

      const_iterator
      cbegin() const noexcept
      {
        return const_iterator(this->_M_impl._M_head->_M_next);
      }

      iterator
      begin() const noexcept
      {
        return iterator(this->_M_impl._M_head->_M_next);
      }

      const_iterator
      cend() const noexcept
      {
        return const_iterator(nullptr);
      }

      iterator
      end() const noexcept
      {
        return iterator(nullptr);
      }
    };


  template <typename _Tp, typename _Pointer>
  std::ostream& operator<<(
    std::ostream& s, const singlylinkedlist::_node<_Tp, _Pointer>& node)
  {
    return s << "Node: data=" << *(node._M_valptr()) << " next=" << node._M_next;
  }

} // namespace pgstl


#endif // PGSTL_SINGLY_LINKED_LIST_HPP

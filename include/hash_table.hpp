#ifndef PGSTL_hash_table_HPP
#define PGSTL_hash_table_HPP

#include <memory>
#include <ostream>
#include <utility>
#include <math.h>

#include "singly_linked_list.hpp"
#include "malloc_allocator.hpp"

namespace pgstl {
	namespace hashtable {
		int _thomas_wang_7_shift(int a)
		{
			a += ~(a << 15);
			a ^= (a >> 10);
			a += (a << 3);
			a ^= (a >> 6);
			a += ~(a << 11);
			a ^= (a >> 16);

			if (a < 0)
				return a * -1;
			else
				return a;
		}
		template <typename T> int _hash(const T& a)
		{
			return _thomas_wang_7_shift((int)a);
		}
		template <typename T> int _hash(const T* a)
		{
			int __result = log(1 + sizeof(T) / log(2) );
			__result = (int)(a) >> __result;


			if (__result < 0)
				return __result * -1;
			else
				return __result;
		}
		template <> int _hash<char>(const char* a)
		{
			int i = 0;
			int __result = 0;
			int __position_fix = 3;
			while (a[i] != '\0')
			{
				__result += _thomas_wang_7_shift((int)a[i] + __position_fix);
				__position_fix += 10;
				++i;
			}

			if (__result < 0)
				return __result * -1;
			else
				return __result;
		}
		template <> int _hash<std::string>(const std::string& a)
		{
			int __result = 0;
			int __position_fix = 3;
			for (std::string::const_iterator it = a.begin(); it != a.end(); ++it)
			{
				__result += _thomas_wang_7_shift((int)*it + __position_fix);
				__position_fix += 10;
			}

			if (__result < 0)
				return __result * -1;
			else
				return __result;
		}

		template <typename _Key, typename _value>
		struct _node {
		public:
			_Key _M_key;
			_value _M_value;
			_node(_Key key, _value val) : _M_key(key), _M_value(val) {}
			_node() {}
		};

		template <typename _Key, typename _value>
		class _hash_table_base {
		protected:
			struct _hash_table_impl {
				singly_linked_list< hashtable::_node<_Key, _value>, malloc_allocator< _node<_Key, _value> > > * _M_list;

				int _M_table_size;
				int _M_random;

				_hash_table_impl(int __size)
				{
					if (!_M_is_prime(__size))
					{
						_M_table_size = _M_next_prime(__size);
					}
					else
					{
						_M_table_size = __size;
					}

					_M_list = new singly_linked_list< _node<_Key, _value>, malloc_allocator< hashtable::_node<_Key, _value> > >[_M_table_size]();
					_M_random = rand() % 100 + 1;
				}
				_hash_table_impl()
				{
					int __size = 2000;
					if (!_M_is_prime(__size))
					{
						_M_table_size = _M_next_prime(__size);
					}
					else
					{
						_M_table_size = __size;
					}

					_M_list = new singly_linked_list< _node<_Key, _value>, malloc_allocator< hashtable::_node<_Key, _value> > >[_M_table_size]();
					_M_random = rand() % 100 + 1;
				}
				~_hash_table_impl()
				{
					delete[] _M_list;
				}
			};

			_hash_table_impl _M_impl;
		public:
			_hash_table_base(const int __size)
				:_M_impl(__size)
			{ }

			~_hash_table_base() = default;

			static int
				_M_next_prime(int __number)
			{
				while (_M_is_prime(++__number)) {}
				return __number;
			}

			static bool
				_M_is_prime(int __number)
			{
				if (__number == 2 || __number == 3)
				{
					return true;
				}
				if (__number % 2 == 0 || __number % 3 == 0)
				{
					return false;
				}
				int __divisor = 6;
				while (__divisor*__divisor - 2 * __divisor + 1 <= __number)
				{
					if (__number % (__divisor - 1) == 0)
					{
						return false;
					}
					if (__number % (__divisor + 1) == 0)
					{
						return false;
					}
					__divisor += 6;
				}
				return true;
			}

			int
				_M_hash_function(_Key __key)
			{
				//std::hash<_Key> __convert;
				int __convert = hashtable::_hash<_Key>(__key);
				int __h1 = __convert % _M_impl._M_table_size;
				int __h2 = 1 + (__convert % (_M_impl._M_table_size - 2));
				return (__h1 + _M_impl._M_random*__h2) % _M_impl._M_table_size;
			}
		};

	}
	// ~namespace hashtable
	template <typename _Key, typename _value>
	class _hash_table
		:protected hashtable::_hash_table_base<_Key, _value>
	{
	public:
		using _M_type = hashtable::_node<_Key, _value>;
		using _M_alloc = malloc_allocator< hashtable::_node<_Key, _value> >;
		using _M_base = hashtable::_hash_table_base<_Key, _value>;

		constexpr
			_hash_table(int __size) : hashtable::_hash_table_base<_Key, _value>(__size)
		{}

		void
			insert(_Key __key, _value __val)
		{
			int __hash = this->_M_hash_function(__key);
			if (!this->_M_impl._M_list[__hash].empty())
			{
				typename singly_linked_list< _M_type, _M_alloc >::iterator it = this->_M_impl._M_list[__hash].begin();
				for (; it != this->_M_impl._M_list[__hash].end(); ++it)
				{
					if (it->value()._M_key == __key)
					{
						it->value()._M_value = __val;
						return;
					}
				}
			}
			this->_M_impl._M_list[__hash].push_front(_M_type(__key, __val));
			return;
		}

		bool
			insert_if_empty(_Key __key, _value __val)
		{
			int __hash = this->_M_hash_function(__key);
			if (!this->_M_impl._M_list[__hash].empty())
			{
				typename singly_linked_list< _M_type, _M_alloc >::iterator it = this->_M_impl._M_list[__hash].begin();
				for (; it != this->_M_impl._M_list[__hash].end(); ++it)
				{
					if (it->value()._M_key == __key)
					{
						return false;
					}
				}
			}
			this->_M_impl._M_list[__hash].push_front(_M_type(__key, __val));
			return true;
		}

		_value
			search(_Key __key)
		{
			int __hash = this->_M_hash_function(__key);

			if (this->_M_impl._M_list[__hash].empty())
			{
				return -1;
			}
			else
			{
				for (auto i = this->_M_impl._M_list[__hash].begin(); i != this->_M_impl._M_list[__hash].end(); i++)
				{
					if (i->value()._M_key == __key)
					{
						return i->value()._M_value;
					}
				}
			}

			return -1;
		}

		bool
			erase(_Key __key)
		{
			int __hash = this->_M_hash_function(__key);

			if (this->_M_impl._M_list[__hash].empty())
			{
				return false;
			}

			if (this->_M_impl._M_list[__hash].begin()->value()._M_key == __key) {
				this->_M_impl._M_list[__hash].pop_front();
				return true;
			}

			auto j = this->_M_impl._M_list[__hash].begin();
			for (auto i = this->_M_impl._M_list[__hash].begin(); i != this->_M_impl._M_list[__hash].end(); i++)
			{

				if (i->value()._M_key == __key)
				{
					this->_M_impl._M_list[__hash].erase_after(j);
					return true;
				}
				j = i;
			}
			return false;
		}
	};
} // ~namespace pgstl
#endif // ~PGSTL_hash_table_HPP

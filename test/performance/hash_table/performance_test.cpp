#include "malloc_allocator.hpp"
#include "array_allocator.hpp"
#include "hash_table.hpp"

#include <chrono>
#include <unordered_map>
#include <iostream>
#include <memory>
#include <fstream>

#include <gtest/gtest.h>

namespace pgstl
{
  namespace test
  {
    class performance_test
      : public ::testing::TestWithParam<std::size_t>
    {
    public:
      void SetUp() override
      {
        _M_start = std::chrono::system_clock::now();
      }

      void TearDown() override
      {
        auto dur =
          std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now()-_M_start).count();

        double op = (double)dur / (_M_loop / GetParam());


        _S_ofile
          << GetParam() << "\t" << op << std::endl;
      }

      std::chrono::time_point<std::chrono::system_clock> _M_start;
      int _M_loop = 1000000;

      static std::ofstream _S_ofile;
    };

    std::ofstream performance_test::_S_ofile;

    class insert_performance_test
      : public performance_test
    {
    public:
      static void
      SetUpTestCase()
      {
        _S_ofile.open("hashtable_insert.dat");
      }

      static void
      TearDownTestCase()
      {
        _S_ofile.close();
      }
    };

    TEST_P(insert_performance_test, std_ref)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      for (int l = 0; l < loop; ++l)
        {
          std::unordered_map<int, int> list;
          for (int i = 0; i < end; ++i)
            {
              list[i] = i;
            }
        }
    }

    TEST_P(insert_performance_test, pgstl_hash_table)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      for (int l = 0; l < loop; ++l)
        {
          _hash_table<int, int> list(end);
          for (int i = 0; i < end; ++i)
            {
              list.insert(i, i);
            }
        }
    }

    INSTANTIATE_TEST_CASE_P(perf, insert_performance_test,
                            ::testing::Values(1, 10, 100, 1000, 10000));

    class search_performance_test
      : public performance_test
    {
    public:
      static void
      SetUpTestCase()
      {
        _S_ofile.open("hashtable_search.dat");
      }

      static void
      TearDownTestCase()
      {
        _S_ofile.close();
      }
    };

    TEST_P(search_performance_test, std_ref)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      std::unordered_map<int, int> list;
      for (int i = 0; i < end; ++i)
        {
          list[i] = i;
        }
      this->_M_start = std::chrono::system_clock::now();

      for (int l = 0; l < loop; ++l)
        {
          for (int i = 0; i < end; ++i)
            {
              list.find(i);
            }
        }
    auto dur =
      std::chrono::duration_cast<std::chrono::microseconds>(
         std::chrono::system_clock::now()-this->_M_start).count();

    double op = (double)dur / (this->_M_loop / GetParam());

    _S_ofile
      << GetParam() << "\t" << op << std::endl;
  }

    TEST_P(search_performance_test, pgstl_hash_table)
    {
        int end = GetParam();
        int loop = _M_loop / end;

        _hash_table<int, int> list(end);
        for (int i = 0; i < end; ++i)
          {
            list.insert(i,i);
          }
        this->_M_start = std::chrono::system_clock::now();

        for (int l = 0; l < loop; ++l)
          {
            for (int i = 0; i < end; ++i)
              {
                list.search(i);
              }
          }
      auto dur =
        std::chrono::duration_cast<std::chrono::microseconds>(
           std::chrono::system_clock::now()-this->_M_start).count();

      double op = (double)dur / (this->_M_loop / GetParam());

      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }
    INSTANTIATE_TEST_CASE_P(perf, search_performance_test,
                            ::testing::Values(1, 10, 100, 1000, 10000));

    class erase_performance_test
      : public performance_test
    {
    public:
      static void
      SetUpTestCase()
      {
        _S_ofile.open("hashtable_erase.dat");
      }

      static void
      TearDownTestCase()
      {
        _S_ofile.close();
      }
    };

    TEST_P(erase_performance_test, std_ref)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      for (int l = 0; l < loop; ++l)
        {
          std::unordered_map<int, int> list;
          for (int i = 0; i < end; ++i)
            {
              list.erase(i);
            }
        }
    }

    TEST_P(erase_performance_test, pgstl_hash_table)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      for (int l = 0; l < loop; ++l)
        {
          _hash_table<int, int> list(end);
          for (int i = 0; i < end; ++i)
            {
              list.erase(i);
            }
        }
    }
    INSTANTIATE_TEST_CASE_P(perf, erase_performance_test,
                            ::testing::Values(1, 10, 100, 1000, 10000));

  } // namespace test
} // namespace pgstl

#include "malloc_allocator.hpp"
#include "array_allocator.hpp"
#include "red_black_tree.hpp"

#include <chrono>
#include <forward_list>
#include <iostream>
#include <memory>
#include <fstream>

#include <gtest/gtest.h>

namespace pgstl
{
  namespace test
  {
    class performance_test
      : public ::testing::TestWithParam<std::size_t>
    {
    public:
      void SetUp() override
      {
          _M_start = std::chrono::high_resolution_clock::now();
      }

      void TearDown() override
      {
        auto dur =
          std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now()-_M_start).count();

        double op = (double)dur / (_M_loop / GetParam());


        _S_ofile
          << GetParam() << "\t" << op << std::endl;
      }

      std::chrono::time_point<std::chrono::high_resolution_clock> _M_start;
      int _M_loop = 1000000;
      static std::ofstream _S_ofile;
    };

    std::ofstream performance_test::_S_ofile;

    class insert_performance_test
      : public performance_test
    {
    public:
      void
      SetUp()
      {
        for (int i = 0; i < GetParam(); ++i)
          {
            _M_test_data.push_back(i);
          }

        std::random_device rd;
        std::mt19937 g(rd());
        std::shuffle(_M_test_data.begin(), _M_test_data.end(), g);

        performance_test::SetUp();
      }

      void
      TearDown()
      {
        performance_test::TearDown();

        _M_test_data.clear();
      }


      static void
      SetUpTestCase()
      {
        _S_ofile.open("rbtree_insert.dat");
      }

      static void
      TearDownTestCase()
      {
        _S_ofile.close();
      }

      std::vector<int> _M_test_data;
    };

    TEST_P(insert_performance_test, pgstl_std_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      std::allocator<int> a;
      for (int l = 0; l < loop; ++l)
        {
          red_black_tree<int, std::less<int>, std::allocator<int>> tree(a);
          for (auto&& v : this->_M_test_data)
            {
              tree.insert(v);
            }
        }
    }

    TEST_P(insert_performance_test, pgstl_malloc_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      malloc_allocator<int> a;
      for (int l = 0; l < loop; ++l)
        {
          red_black_tree<int, std::less<int>, malloc_allocator<int>> tree(a);
          for (auto&& v : this->_M_test_data)
            {
              tree.insert(v);
            }
        }
    }

    TEST_P(insert_performance_test, pgstl_array_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      array_storage ary;
      using alloc_type = array_allocator<int>;

      alloc_type a(&ary);
      for (int l = 0; l < loop; ++l)
        {
          red_black_tree<int, std::less<int>, array_allocator<int>> tree(a);
          for (auto&& v : this->_M_test_data)
            {
              tree.insert(v);
            }
          ary.reset();
        }
    }

    TEST_P(insert_performance_test, pgstl_array_alloc_indirect_addr)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      array_storage ary;
      using alloc_type = array_allocator<int, array_storage, array_pointer<int>>;

      alloc_type a(&ary);
      for (int l = 0; l < loop; ++l)
        {
          red_black_tree<int, std::less<int>, alloc_type> tree(a);
          for (auto&& v : this->_M_test_data)
            {
              tree.insert(v);
            }
          ary.reset();
        }
    }

    INSTANTIATE_TEST_CASE_P(perf, insert_performance_test,
                            ::testing::Values(1, 10, 100, 1000, 10000));

    class iterate_performance_test
      : public performance_test
    {
    public:
      void
      SetUp()
      {
        for (int i = 0; i < GetParam(); ++i)
          {
            _M_test_data.push_back(i);
          }

        std::random_device rd;
        std::mt19937 g(rd());
        std::shuffle(_M_test_data.begin(), _M_test_data.end(), g);

        performance_test::SetUp();
      }

      void
      TearDown()
      {
        performance_test::TearDown();

        _M_test_data.clear();
      }


      static void
      SetUpTestCase()
      {
        _S_ofile.open("rbtree_iterate.dat");
      }

      static void
      TearDownTestCase()
      {
        _S_ofile.close();
      }

      std::vector<int> _M_test_data;
    };

    TEST_P(iterate_performance_test, pgstl_std_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      using tree_type = red_black_tree<int, std::less<int>, std::allocator<int>>;
      std::allocator<int> a;
      tree_type tree(a);
      for (auto&& v : this->_M_test_data)
        {
          tree.insert(v);
        }

      this->_M_start = std::chrono::high_resolution_clock::now();
      for (int l = 0; l < loop; ++l)
        {
          for (tree_type::iterator i = tree.begin(); i != tree.end(); ++i)
            {
              (void)*i;
            }
        }
      auto dur =
        std::chrono::duration_cast<std::chrono::microseconds>(
           std::chrono::high_resolution_clock::now()-this->_M_start).count();

      double op = (double)dur / (this->_M_loop / GetParam());


      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(iterate_performance_test, pgstl_malloc_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      malloc_allocator<int> a;
      using tree_type = red_black_tree<int, std::less<int>, malloc_allocator<int>>;
      tree_type tree(a);
      for (auto&& v : this->_M_test_data)
        {
          tree.insert(v);
        }

      this->_M_start = std::chrono::high_resolution_clock::now();
      for (int l = 0; l < loop; ++l)
        {
          for (tree_type::iterator i = tree.begin(); i != tree.end(); ++i)
            {
              (void)*i;
            }
        }
      auto dur =
        std::chrono::duration_cast<std::chrono::microseconds>(
           std::chrono::high_resolution_clock::now()-this->_M_start).count();

      double op = (double)dur / (this->_M_loop / GetParam());


      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(iterate_performance_test, pgstl_array_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      array_storage ary;
      using alloc_type = array_allocator<int>;

      alloc_type a(&ary);
      using tree_type = red_black_tree<int, std::less<int>, array_allocator<int>>;
      tree_type tree(a);
      for (auto&& v : this->_M_test_data)
        {
          tree.insert(v);
        }

      this->_M_start = std::chrono::high_resolution_clock::now();
      for (int l = 0; l < loop; ++l)
        {
          for (tree_type::iterator i = tree.begin(); i != tree.end(); ++i)
            {
              (void)*i;
            }
        }
      auto dur =
        std::chrono::duration_cast<std::chrono::microseconds>(
           std::chrono::high_resolution_clock::now()-this->_M_start).count();

      double op = (double)dur / (this->_M_loop / GetParam());


      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(iterate_performance_test, pgstl_array_alloc_indirect_addr)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      array_storage ary;
      using alloc_type = array_allocator<int, array_storage, array_pointer<int>>;

      alloc_type a(&ary);
      using tree_type = red_black_tree<int, std::less<int>, alloc_type>;
      tree_type tree(a);
      for (auto&& v : this->_M_test_data)
        {
          tree.insert(v);
        }

      this->_M_start = std::chrono::high_resolution_clock::now();
      for (int l = 0; l < loop; ++l)
        {
          for (tree_type::iterator i = tree.begin(); i != tree.end(); ++i)
            {
              (void)*i;
            }
        }
      auto dur =
        std::chrono::duration_cast<std::chrono::microseconds>(
           std::chrono::high_resolution_clock::now()-this->_M_start).count();

      double op = (double)dur / (this->_M_loop / GetParam());


      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    INSTANTIATE_TEST_CASE_P(perf, iterate_performance_test,
                            ::testing::Values(1, 10, 100, 1000, 10000));

  } // namespace test
} // namespace pgstl

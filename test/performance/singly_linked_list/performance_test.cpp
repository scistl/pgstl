#include "malloc_allocator.hpp"
#include "array_allocator.hpp"
#include "singly_linked_list.hpp"

#include <chrono>
#include <forward_list>
#include <iostream>
#include <memory>
#include <fstream>

#include <gtest/gtest.h>

namespace pgstl
{
  namespace test
  {
    class performance_test
      : public ::testing::TestWithParam<std::size_t>
    {
    public:
      void SetUp() override
      {
        _M_start = std::chrono::high_resolution_clock::now();
      }

      void TearDown() override
      {
        auto dur =
          std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::high_resolution_clock::now()-_M_start).count();

        double op = (double)dur / (_M_loop / GetParam());


        _S_ofile
          << GetParam() << "\t" << op << std::endl;
      }

      std::chrono::time_point<std::chrono::high_resolution_clock> _M_start;
      int _M_loop = 10000000;

      static std::ofstream _S_ofile;
    };

    std::ofstream performance_test::_S_ofile;

    class push_front_performance_test
      : public performance_test
    {
    public:
      static void
      SetUpTestCase()
      {
        _S_ofile.open("slist_push_front.dat");
      }

      static void
      TearDownTestCase()
      {
        _S_ofile.close();
      }
    };

    TEST_P(push_front_performance_test, std_ref)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      for (int l = 0; l < loop; ++l)
        {
          std::forward_list<int> list;
          for (int i = 0; i < end; ++i)
            {
              list.push_front(i);
            }
        }
    }

    TEST_P(push_front_performance_test, pgstl_std_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      for (int l = 0; l < loop; ++l)
        {
          singly_linked_list<int> list;
          for (int i = 0; i < end; ++i)
            {
              list.push_front(i);
            }
        }
    }

    TEST_P(push_front_performance_test, pgstl_malloc_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      for (int l = 0; l < loop; ++l)
        {
          singly_linked_list<int, malloc_allocator<int>> list;
          for (int i = 0; i < end; ++i)
            {
              list.push_front(i);
            }
        }
    }

    TEST_P(push_front_performance_test, pgstl_array_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      array_storage ary;
      for (int l = 0; l < loop; ++l)
        {
          using alloc_type = array_allocator<int>;

          alloc_type alc(&ary);
          singly_linked_list<int, alloc_type> list(alc);
          for (int i = 0; i < end; ++i)
            {
              list.push_front(i);
            }

          ary.reset();
        }
    }

    TEST_P(push_front_performance_test, pgstl_array_alloc_indirect_addr)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      array_storage ary;
      for (int l = 0; l < loop; ++l)
        {
          using alloc_type =
            array_allocator<int, array_storage, array_pointer<int>>;

          alloc_type alc(&ary);
          singly_linked_list<int, alloc_type> list(alc);
          for (int i = 0; i < end; ++i)
            {
              list.push_front(i);
            }
          ary.reset();
        }
    }

    INSTANTIATE_TEST_CASE_P(perf, push_front_performance_test,
                            ::testing::Values(1, 10, 100, 1000, 10000));

    class iterate_performance_test
      : public performance_test
    {
    public:
      static void
      SetUpTestCase()
      {
        _S_ofile.open("slist_iterate.dat");
      }

      static void
      TearDownTestCase()
      {
        _S_ofile.close();
      }

      void
      SetUp() override
      { }

      void
      TearDown() override
      { }
    };

    TEST_P(iterate_performance_test, std_ref)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      std::forward_list<int> list;
      for (int i = 0; i < end; ++i)
        {
          list.push_front(i);
        }

      this->_M_start = std::chrono::high_resolution_clock::now();
      for (int l = 0; l < loop; ++l)
        {
          for (std::forward_list<int>::iterator i = list.begin(); i != list.end(); ++i)
            {
              (void)*i;
            }
        }
      auto dur =
        std::chrono::duration_cast<std::chrono::microseconds>(
           std::chrono::high_resolution_clock::now()-this->_M_start).count();

      double op = (double)dur / (this->_M_loop / GetParam());


      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(iterate_performance_test, pgstl_std_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      using list_type = pgstl::singly_linked_list<int>;
      list_type list;
      for (int i = 0; i < end; ++i)
        {
          list.push_front(i);
        }

      this->_M_start = std::chrono::high_resolution_clock::now();
      for (int l = 0; l < loop; ++l)
        {
          for (list_type::iterator i = list.begin(); i != list.end(); ++i)
            {
              (void)*i;
            }
        }
      auto dur =
        std::chrono::duration_cast<std::chrono::microseconds>(
           std::chrono::high_resolution_clock::now()-this->_M_start).count();

      double op = (double)dur / (this->_M_loop / GetParam());


      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(iterate_performance_test, pgstl_malloc_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      using list_type = pgstl::singly_linked_list<int, malloc_allocator<int>>;
      list_type list;
      for (int i = 0; i < end; ++i)
        {
          list.push_front(i);
        }

      this->_M_start = std::chrono::high_resolution_clock::now();
      for (int l = 0; l < loop; ++l)
        {
          for (list_type::iterator i = list.begin(); i != list.end(); ++i)
            {
              (void)*i;
            }
        }
      auto dur =
        std::chrono::duration_cast<std::chrono::microseconds>(
           std::chrono::high_resolution_clock::now()-this->_M_start).count();

      double op = (double)dur / (this->_M_loop / GetParam());


      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(iterate_performance_test, pgstl_array_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      using alloc_type = array_allocator<int, array_storage>;

      array_storage ary;
      alloc_type alc(&ary);
      using list_type = pgstl::singly_linked_list<int, alloc_type>;
      list_type list(alc);

      for (int i = 0; i < end; ++i)
        {
          list.push_front(i);
        }

      this->_M_start = std::chrono::high_resolution_clock::now();
      for (int l = 0; l < loop; ++l)
        {
          for (list_type::iterator i = list.begin(); i != list.end(); ++i)
            {
              (void)*i;
            }
        }
      auto dur =
        std::chrono::duration_cast<std::chrono::microseconds>(
           std::chrono::high_resolution_clock::now()-this->_M_start).count();

      double op = (double)dur / (this->_M_loop / GetParam());


      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(iterate_performance_test, pgstl_array_alloc_indirect_addr)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      using alloc_type =
        array_allocator<int, array_storage, array_pointer<int>>;

      array_storage ary;
      alloc_type alc(&ary);
      using list_type = singly_linked_list<int, alloc_type>;
      list_type list(alc);

      for (int i = 0; i < end; ++i)
        {
          list.push_front(i);
        }

      this->_M_start = std::chrono::high_resolution_clock::now();
      for (int l = 0; l < loop; ++l)
        {
          for (list_type::iterator i = list.begin(); i != list.end(); ++i)
            {
              (void)*i;
            }
        }
      auto dur =
        std::chrono::duration_cast<std::chrono::microseconds>(
           std::chrono::high_resolution_clock::now()-this->_M_start).count();

      double op = (double)dur / (this->_M_loop / GetParam());


      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    INSTANTIATE_TEST_CASE_P(perf, iterate_performance_test,
                            ::testing::Values(1, 10, 100, 1000, 10000));


    // FIXME(ajedrych) something is crashing here investigate
#if  0
    class erase_performance_test
      : public performance_test
    {
    public:
      static void
      SetUpTestCase()
      {
        _S_ofile.open("slist_erase.dat");
      }

      static void
      TearDownTestCase()
      {
        _S_ofile.close();
      }

      void
      SetUp() override
      { }

      void
      TearDown() override
      { }
    };

    TEST_P(erase_performance_test, std_ref)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      std::forward_list<int> list;

      int dur = 0;

      for (int k = 0; k < loop; ++k)
        {
          for (int i = 0; i < end; ++i)
            {
              list.push_front(i);
            }

          this->_M_start = std::chrono::high_resolution_clock::now();
          for (int l = 0; l < end; ++l)
            {
              list.pop_front();
            }
          dur +=
            std::chrono::duration_cast<std::chrono::microseconds>(
              std::chrono::high_resolution_clock::now()-this->_M_start).count();
        }

      double op = (double)dur / loop;

      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(erase_performance_test, pgstl_std_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      using list_type = pgstl::singly_linked_list<int>;
      list_type list;

      int dur = 0;

      for (int k = 0; k < loop; ++k)
        {
          for (int i = 0; i < end; ++i)
            {
              list.push_front(i);
            }

          this->_M_start = std::chrono::high_resolution_clock::now();
          for (int l = 0; l < end; ++l)
            {
              list.pop_front();
            }
          dur +=
            std::chrono::duration_cast<std::chrono::microseconds>(
              std::chrono::high_resolution_clock::now()-this->_M_start).count();
        }

      double op = (double)dur / loop;

      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(erase_performance_test, pgstl_malloc_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      using list_type = pgstl::singly_linked_list<int, malloc_allocator<int>>;
      list_type list;

      int dur = 0;

      for (int k = 0; k < loop; ++k)
        {
          for (int i = 0; i < end; ++i)
            {
              list.push_front(i);
            }

          this->_M_start = std::chrono::high_resolution_clock::now();
          for (int l = 0; l < end; ++l)
            {
              list.pop_front();
            }
          dur +=
            std::chrono::duration_cast<std::chrono::microseconds>(
              std::chrono::high_resolution_clock::now()-this->_M_start).count();
        }

      double op = (double)dur / loop;

      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(erase_performance_test, pgstl_array_alloc)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      using alloc_type = array_allocator<int, array_storage>;

      array_storage ary;
      alloc_type alc(&ary);
      using list_type = pgstl::singly_linked_list<int, alloc_type>;
      list_type list(alc);

      int dur = 0;

      for (int k = 0; k < loop; ++k)
        {
          for (int i = 0; i < end; ++i)
            {
              list.push_front(i);
            }

          this->_M_start = std::chrono::high_resolution_clock::now();
          for (int l = 0; l < end; ++l)
            {
              list.pop_front();
            }
          dur +=
            std::chrono::duration_cast<std::chrono::microseconds>(
              std::chrono::high_resolution_clock::now()-this->_M_start).count();
        }

      double op = (double)dur / loop;

      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    TEST_P(erase_performance_test, pgstl_array_alloc_indirect_addr)
    {
      int end = GetParam();
      int loop = _M_loop / end;

      using alloc_type =
        array_allocator<int, array_storage, array_pointer<int>>;

      array_storage ary;
      alloc_type alc(&ary);
      using list_type = singly_linked_list<int, alloc_type>;
      list_type list(alc);

      int dur = 0;

      for (int k = 0; k < loop; ++k)
        {
          for (int i = 0; i < end; ++i)
            {
              list.push_front(i);
            }

          this->_M_start = std::chrono::high_resolution_clock::now();
          for (int l = 0; l < end; ++l)
            {
              list.pop_front();
            }
          dur +=
            std::chrono::duration_cast<std::chrono::microseconds>(
              std::chrono::high_resolution_clock::now()-this->_M_start).count();
        }

      double op = (double)dur / loop;

      _S_ofile
        << GetParam() << "\t" << op << std::endl;
    }

    INSTANTIATE_TEST_CASE_P(perf, erase_performance_test,
                            ::testing::Values(1, 10, 100, 1000, 10000));
#endif
  } // namespace test
} // namespace pgstl

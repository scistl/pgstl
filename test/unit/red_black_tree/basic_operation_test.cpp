#include <random>
#include <algorithm>
#include <iterator>
#include <cstdlib>

#include <gtest/gtest.h>

#include "array_allocator.hpp"
#include "malloc_allocator.hpp"

#include "red_black_tree.hpp"

#include "testing/make_alloc.hpp"


namespace pgstl
{
  namespace test
  {

    template <typename _Alloc>
    class basic_operation_test
      : public ::testing::Test
    {
    public:
      using alloc_type = _Alloc;
      using value_type = typename alloc_type::value_type;
      using pointer    = typename alloc_type::pointer;
      using comp_type  = std::less<value_type>;
      using tree_type  = red_black_tree<value_type, comp_type, alloc_type>;

    protected:
      void
      SetUp() override
      {
        _M_alloc = testing::alloc_factory<_Alloc>::make();
        _M_tree  = new tree_type(*_M_alloc);
      }

      void
      TearDown() override
      {
        delete _M_tree;
        delete _M_alloc;
      }

    protected:
      alloc_type* _M_alloc;
      tree_type*  _M_tree;
    };

    using alloc_types = ::testing::Types<
      std::allocator<int>,
      malloc_allocator<int>,
      array_allocator<int>,
      array_allocator<int, array_storage, array_pointer<int>>,
      array_allocator<int, array_storage, array_fixed_pointer<int>>>;

    TYPED_TEST_CASE(basic_operation_test, alloc_types);

    TYPED_TEST(basic_operation_test, construction)
    {
      EXPECT_TRUE(this->_M_tree->empty());
      EXPECT_EQ(0, this->_M_tree->size());
    }

    TYPED_TEST(basic_operation_test, insert)
    {
      EXPECT_EQ(0, this->_M_tree->size());

      for (int i = 0; i < 1000; ++i)
        {
          this->_M_tree->insert(std::rand());
        }

      EXPECT_EQ(1000, this->_M_tree->size());
    }

    TYPED_TEST(basic_operation_test, empty)
    {
      EXPECT_TRUE(this->_M_tree->empty());

      this->_M_tree->insert(std::rand());

      EXPECT_FALSE(this->_M_tree->empty());
    }

    TYPED_TEST(basic_operation_test, iteration)
    {
      std::vector<int> test_data;

      EXPECT_EQ(0, this->_M_tree->size());

      for (int i = 0; i < 1000; ++i)
        {
          test_data.push_back(i);
        }

      std::vector<int> scrambled = test_data;
      std::random_device rd;
      std::mt19937 g(rd());
      std::shuffle(scrambled.begin(), scrambled.end(), g);

      for (auto&& v : scrambled)
        {
          this->_M_tree->insert(v);
        }

      EXPECT_EQ(1000, this->_M_tree->size());

      int k = 0;
      for (typename basic_operation_test<TypeParam>::tree_type::iterator i = this->_M_tree->begin();
           i != this->_M_tree->end(); ++i)
        {
          EXPECT_EQ(test_data[k++], i->key());
        }
    }

    TYPED_TEST(basic_operation_test, erase)
    {
      EXPECT_EQ(0, this->_M_tree->size());

      for (int i = 0; i < 1000; ++i)
        {
          this->_M_tree->insert(std::rand());
        }

      EXPECT_EQ(1000, this->_M_tree->size());

      int k = 0;
      while (!this->_M_tree->empty())
        {
          ++k;
          this->_M_tree->erase(this->_M_tree->begin());
        }

      EXPECT_EQ(0, this->_M_tree->size());
      EXPECT_EQ(1000, k);
    }

    TYPED_TEST(basic_operation_test, erase_half)
    {
      std::vector<int> test_data;

      EXPECT_EQ(0, this->_M_tree->size());

      for (int i = 0; i < 1000; ++i)
        {
          test_data.push_back(i);
        }

      std::vector<int> scrambled = test_data;
      std::random_device rd;
      std::mt19937 g(rd());
      std::shuffle(scrambled.begin(), scrambled.end(), g);

      for (auto&& v : scrambled)
        {
          this->_M_tree->insert(v);
        }

      EXPECT_EQ(1000, this->_M_tree->size());

      for (int e = 0; e < 500; ++e)
        {
          this->_M_tree->erase(this->_M_tree->begin());
        }

      EXPECT_EQ(500, this->_M_tree->size());

      int k = 500;
      for (typename basic_operation_test<TypeParam>::tree_type::iterator i = this->_M_tree->begin();
           i != this->_M_tree->end(); ++i)
        {
          EXPECT_EQ(test_data[k++], i->key());
        }
    }
  } // ~namespace test
} // ~namespace pgstl

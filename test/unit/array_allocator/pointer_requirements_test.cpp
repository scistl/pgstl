#include <gtest/gtest.h>

#include "array_allocator.hpp"

#include "testing/nullable_pointer_concept.hpp"
#include "testing/nullable_const_pointer_concept.hpp"
#include "testing/nullable_void_pointer_concept.hpp"
#include "testing/nullable_const_void_pointer_concept.hpp"
#include "testing/random_access_iterator_concept.hpp"


using namespace pgstl::testing;

namespace pgstl
{
  namespace test
  {
    INSTANTIATE_TYPED_TEST_CASE_P(pointer, nullable_pointer_concept,
                                  array_allocator<int>);

    INSTANTIATE_TYPED_TEST_CASE_P(const_pointer, nullable_const_pointer_concept,
                                  array_allocator<int>);

    INSTANTIATE_TYPED_TEST_CASE_P(void_pointer, nullable_void_pointer_concept,
                                  array_allocator<int>);

    INSTANTIATE_TYPED_TEST_CASE_P(const_void_pointer, nullable_const_void_pointer_concept,
                                  array_allocator<int>);

    INSTANTIATE_TYPED_TEST_CASE_P(random_iterator, random_access_iterator_concept,
                                  array_allocator<int>);
  } // namespace test
} // namespace pgstl

#include <gtest/gtest.h>

#include "array_allocator.hpp"
#include "malloc_allocator.hpp"

#include "singly_linked_list.hpp"

#include "testing/make_alloc.hpp"


namespace pgstl
{
  namespace test
  {
    template <typename _Alloc>
    class basic_operation_test
      : public ::testing::Test
    {
    public:
      using alloc_type = _Alloc;
      using value_type = typename alloc_type::value_type;
      using pointer    = typename alloc_type::pointer;
      using list_type  = singly_linked_list<value_type, alloc_type>;

    protected:
      void
      SetUp() override
      {
        _M_alloc = testing::alloc_factory<_Alloc>::make();
        _M_list  = new list_type(*_M_alloc);
      }

      void
      TearDown() override
      {
        delete _M_list;
        delete _M_alloc;
      }

    protected:
      alloc_type* _M_alloc;
      list_type*  _M_list;

    };

    using alloc_types = ::testing::Types<
      std::allocator<int>,
      malloc_allocator<int>,
      array_allocator<int>,
      array_allocator<int, array_storage, array_pointer<int>>,
      array_allocator<int, array_storage, array_fixed_pointer<int>>>;
    TYPED_TEST_CASE(basic_operation_test, alloc_types);


    TYPED_TEST(basic_operation_test, construction)
    {
      EXPECT_TRUE(this->_M_list->empty());
    }

    TYPED_TEST(basic_operation_test, insert_after)
    {
      std::array<int,4> test_data = { 1, 2, 3, 4 };
      std::array<int,3> input_data = { 4, 3, 1 };

      EXPECT_TRUE(this->_M_list->empty());

      // NOTICE(ajedrych) Prepare test data
      for (std::array<int,4>::iterator i = input_data.begin();
           i != input_data.end(); ++i)
        {
          this->_M_list->push_front(*i);
        }

      EXPECT_FALSE(this->_M_list->empty());
      this->_M_list->insert_after(this->_M_list->begin(), 2);

      // NOTICE(ajedrych) Actual test
      auto ai = this->_M_list->begin();
      for (std::array<int,4>::iterator i = test_data.begin();
           i != test_data.end(); ++i, ++ai)
        {
          EXPECT_EQ(*i, ai->value());
        }

      EXPECT_EQ(ai, this->_M_list->end());

      this->_M_list->clear();
      EXPECT_TRUE(this->_M_list->empty());
    }


    TYPED_TEST(basic_operation_test, erase_after)
    {
      std::array<int,4> test_data = { 1, 2, 3, 4 };
      std::array<int,5> input_data = { 4, 3, 2, 0, 1 };

      EXPECT_TRUE(this->_M_list->empty());

      // NOTICE(ajedrych) Prepare test data
      for (std::array<int,4>::iterator i = input_data.begin();
           i != input_data.end(); ++i)
        {
          this->_M_list->push_front(*i);
        }

      EXPECT_FALSE(this->_M_list->empty());
      this->_M_list->erase_after(this->_M_list->begin());

      // NOTICE(ajedrych) Actual test
      auto ai = this->_M_list->begin();
      for (std::array<int,4>::iterator i = test_data.begin();
           i != test_data.end(); ++i, ++ai)
        {
          EXPECT_EQ(*i, ai->value());
        }

      EXPECT_EQ(ai, this->_M_list->end());

      this->_M_list->clear();
      EXPECT_TRUE(this->_M_list->empty());
    }

    TYPED_TEST(basic_operation_test, push_front)
    {
      EXPECT_TRUE(this->_M_list->empty());

      this->_M_list->push_front(1);
      EXPECT_FALSE(this->_M_list->empty());

      this->_M_list->clear();
      EXPECT_TRUE(this->_M_list->empty());
    }

    TYPED_TEST(basic_operation_test, pop_front)
    {
      pgstl::singly_linked_list<int> a;
      EXPECT_TRUE(this->_M_list->empty());

      this->_M_list->push_front(1);
      EXPECT_FALSE(this->_M_list->empty());

      this->_M_list->pop_front();
      EXPECT_TRUE(this->_M_list->empty());

      this->_M_list->clear();
      EXPECT_TRUE(this->_M_list->empty());
    }

    TYPED_TEST(basic_operation_test, head)
    {
      EXPECT_TRUE(this->_M_list->empty());

      this->_M_list->push_front(1);
      EXPECT_FALSE(this->_M_list->empty());

      EXPECT_EQ(1, this->_M_list->head().value());

      this->_M_list->clear();
      EXPECT_TRUE(this->_M_list->empty());
    }

    TYPED_TEST(basic_operation_test, head_output)
    {
      EXPECT_TRUE(this->_M_list->empty());

      this->_M_list->push_front(1);
      EXPECT_FALSE(this->_M_list->empty());

      std::cout << this->_M_list->head() << std::endl;
      this->_M_list->push_front(2);
      std::cout << this->_M_list->head() << std::endl;
      this->_M_list->push_front(3);
      std::cout << this->_M_list->head() << std::endl;
      this->_M_list->push_front(4);
      std::cout << this->_M_list->head() << std::endl;
      this->_M_list->push_front(5);
      std::cout << this->_M_list->head() << std::endl;

      EXPECT_FALSE(this->_M_list->empty());

      this->_M_list->clear();
      EXPECT_TRUE(this->_M_list->empty());
    }

    TYPED_TEST(basic_operation_test, iteration)
    {
      std::array<int,4> test_data = { 1, 2, 3, 4 };

      EXPECT_TRUE(this->_M_list->empty());

      // NOTICE(ajedrych) Prepare test data
      for (std::array<int,4>::iterator i = test_data.begin();
           i != test_data.end(); ++i)
        {
          this->_M_list->push_front(*i);
        }

      EXPECT_FALSE(this->_M_list->empty());

      // NOTICE(ajedrych) Actual test
      auto ai = this->_M_list->begin();
      for (std::array<int,4>::reverse_iterator i = test_data.rbegin();
           i != test_data.rend(); ++i, ++ai)
        {
          EXPECT_EQ(*i, ai->value());
        }

      EXPECT_EQ(ai, this->_M_list->end());

      this->_M_list->clear();
      EXPECT_TRUE(this->_M_list->empty());
    }

    TYPED_TEST(basic_operation_test, const_iteration)
    {
      std::array<int,4> test_data = { 1, 2, 3, 4 };

      EXPECT_TRUE(this->_M_list->empty());

      // NOTICE(ajedrych) Prepare test data
      for (std::array<int,4>::iterator i = test_data.begin();
           i != test_data.end(); ++i)
        {
          this->_M_list->push_front(*i);
        }

      EXPECT_FALSE(this->_M_list->empty());

      // NOTICE(ajedrych) Actual test
      auto ai = this->_M_list->cbegin();
      for (std::array<int,4>::reverse_iterator i = test_data.rbegin();
           i != test_data.rend(); ++i, ++ai)
        {
          EXPECT_EQ(*i, ai->value());
        }

      EXPECT_EQ(ai, this->_M_list->cend());

      this->_M_list->clear();
      EXPECT_TRUE(this->_M_list->empty());
    }
  } // namespace test
} // namespace pgstl

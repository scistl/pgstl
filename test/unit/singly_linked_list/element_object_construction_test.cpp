#include <gtest/gtest.h>

#include "singly_linked_list.hpp"
#include "array_allocator.hpp"
#include "malloc_allocator.hpp"

#include "testing/make_alloc.hpp"


namespace pgstl
{
  namespace test
  {
    struct test_obj
    {
      test_obj()
      {
        ++_M_refcount;
      }

      test_obj(const test_obj&)
      {
        ++_M_refcount;
      }

      ~test_obj()
      {
        --_M_refcount;
      }

      static std::size_t _M_refcount;
    };

    std::size_t test_obj::_M_refcount = 0;

    template <typename _Alloc>
    class element_object_construction_test
      : public ::testing::Test
    {
    public:
      using alloc_type = _Alloc;
      using value_type = typename alloc_type::value_type;
      using pointer    = typename alloc_type::pointer;
      using list_type  = singly_linked_list<value_type, alloc_type>;

    protected:
      void
      SetUp() override
      {
        _M_alloc = testing::alloc_factory<_Alloc>::make();
        _M_list  = new list_type(*_M_alloc);
      }

      void
      TearDown() override
      {
        delete _M_list;
        delete _M_alloc;
      }

    protected:
      alloc_type* _M_alloc;
      list_type*  _M_list;

    };

    using alloc_types = ::testing::Types<
      std::allocator<test_obj>,
      malloc_allocator<test_obj>,
      array_allocator<test_obj>,
      array_allocator<int, array_storage, array_pointer<int>>>;

    TYPED_TEST_CASE(element_object_construction_test, alloc_types);

    TYPED_TEST(element_object_construction_test, object_constuction_destruction)
    {
      pgstl::singly_linked_list<test_obj> a;
      ASSERT_EQ(0, test_obj::_M_refcount);

      EXPECT_TRUE(a.empty());

      a.push_front(test_obj());
      EXPECT_FALSE(a.empty());

      a.push_front(test_obj());
      a.push_front(test_obj());
      a.push_front(test_obj());
      a.push_front(test_obj());
      EXPECT_FALSE(a.empty());

      a.clear();
      EXPECT_TRUE(a.empty());

      ASSERT_EQ(0, test_obj::_M_refcount);

      a.push_front(test_obj());
      EXPECT_FALSE(a.empty());
      a.push_front(test_obj());
      a.push_front(test_obj());
      a.push_front(test_obj());
      a.push_front(test_obj());

      a.clear();
      EXPECT_TRUE(a.empty());
      ASSERT_EQ(0, test_obj::_M_refcount);
    }
  } // namespace test
} // namespace pgstl

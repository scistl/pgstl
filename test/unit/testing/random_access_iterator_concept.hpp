#ifndef PGSTL_TESTING_RANDOM_ACCESS_ITERATOR_CONCEPT_HPP
#define PGSTL_TESTING_RANDOM_ACCESS_ITERATOR_CONCEPT_HPP

#include <gtest/gtest.h>

#include "array_allocator.hpp"
#include "malloc_allocator.hpp"

#include "testing/make_alloc.hpp"


namespace pgstl
{
  namespace testing
  {
    template <typename _Alloc>
    class random_access_iterator_concept
      : public ::testing::Test
    {
    public:
      using alloc_type =
        typename std::allocator_traits<_Alloc>::template rebind_alloc<int>;
      using alloc_traits = std::allocator_traits<alloc_type>;
      using pointer = typename std::allocator_traits<alloc_type>::pointer;

      void
      SetUp() override
      {
        _M_alloc = testing::alloc_factory<alloc_type>::make();
      }

      void
      TearDown() override
      {
        delete _M_alloc;
      }

      alloc_type* _M_alloc;
    };

    TYPED_TEST_CASE_P(random_access_iterator_concept);

    TYPED_TEST_P(random_access_iterator_concept, requirements)
    {
      using alloc_traits =
        typename random_access_iterator_concept<TypeParam>::alloc_traits;
      using pointer =
        typename random_access_iterator_concept<TypeParam>::pointer;

      {
        // 1.
        std::ptrdiff_t n = 4;
        pointer r = alloc_traits::allocate(*(this->_M_alloc), 1);
        r += n;
      }

      {
        // 2.
        std::ptrdiff_t n = 4;
        pointer a = alloc_traits::allocate(*(this->_M_alloc), 1);
        a + n;
        n + a;
      }

      {
        // 3.
        std::ptrdiff_t n = 4;
        pointer r = alloc_traits::allocate(*(this->_M_alloc), 1);
        r -= n;
      }

      {
        // 4.
        std::ptrdiff_t n = 4;
        pointer i = alloc_traits::allocate(*(this->_M_alloc), 1);
        i - n;
      }

      {
        // 5.
        pointer a = alloc_traits::allocate(*(this->_M_alloc), 1);
        pointer b = alloc_traits::allocate(*(this->_M_alloc), 1);
        b - a;
      }

      {
        // 6.
        std::ptrdiff_t n = 4;
        pointer i = alloc_traits::allocate(*(this->_M_alloc), 1);
        i[n];
      }

      {
        // 7.
        pointer a = alloc_traits::allocate(*(this->_M_alloc), 1);
        pointer b = alloc_traits::allocate(*(this->_M_alloc), 1);
        a < b;
      }

      {
        // 8.
        pointer a = alloc_traits::allocate(*(this->_M_alloc), 1);
        pointer b = alloc_traits::allocate(*(this->_M_alloc), 1);
        a > b;
      }

      {
        // 9.
        pointer a = alloc_traits::allocate(*(this->_M_alloc), 1);
        pointer b = alloc_traits::allocate(*(this->_M_alloc), 1);
        a >= b;
      }

      {
        // 10.
        pointer a = alloc_traits::allocate(*(this->_M_alloc), 1);
        pointer b = alloc_traits::allocate(*(this->_M_alloc), 1);
        a <= b;
      }
    }

    REGISTER_TYPED_TEST_CASE_P(random_access_iterator_concept,
                               requirements);
  } // ~namespace testing
} // ~namespace pgstl

#endif // PGSTL_TESTING_RANDOM_ACCESS_ITERATOR_CONCEPT_HPP

#ifndef PGSTL_MAKE_ALLOC_HPP
#define PGSTL_MAKE_ALLOC_HPP

#include "array_allocator.hpp"

namespace pgstl
{
  namespace testing
  {
    template <typename _Alloc>
    struct alloc_factory
    {
      static inline _Alloc*
      make()
      {
        return new _Alloc;
      }
    };

    template <typename ..._Tp>
    struct alloc_factory<array_allocator<_Tp...>>
    {
      static inline  array_allocator<_Tp...>*
      make()
      {
        static array_storage storage;
        return new array_allocator<_Tp...>(&storage);
      }
    };
  } // namespace testing
} // namespace pgstl

#endif // PGSTL_MAKE_ALLOC_HPP

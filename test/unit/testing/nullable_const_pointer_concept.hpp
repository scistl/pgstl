#ifndef PGSTL_TESTING_NULLABLE_CONST_POINTER_CONCEPT
#define PGSTL_TESTING_NULLABLE_CONST_POINTER_CONCEPT

#include <gtest/gtest.h>

#include "array_allocator.hpp"
#include "malloc_allocator.hpp"

#include "testing/make_alloc.hpp"


namespace pgstl
{
  namespace testing
  {
    template <typename _Alloc>
    class nullable_const_pointer_concept
      : public ::testing::Test
    {
    public:
      using alloc_type =
        typename std::allocator_traits<_Alloc>::template rebind_alloc<int>;
      using alloc_traits = std::allocator_traits<alloc_type>;
      using pointer = typename std::allocator_traits<alloc_type>::const_pointer;

      void
      SetUp() override
      {
        _M_alloc = testing::alloc_factory<alloc_type>::make();
      }

      void
      TearDown() override
      {
        delete _M_alloc;
      }

      alloc_type* _M_alloc;
    };

    TYPED_TEST_CASE_P(nullable_const_pointer_concept);

    TYPED_TEST_P(nullable_const_pointer_concept, equality_comparable)
    {
      using alloc_traits =
        typename nullable_const_pointer_concept<TypeParam>::alloc_traits;

      auto a = alloc_traits::allocate(*(this->_M_alloc), 1);

      {
        // 1.
        EXPECT_TRUE(a == a);
      }

      {
        const auto b = a;
        // 2.
        EXPECT_TRUE(a == b);
        EXPECT_TRUE(b == a);
      }

      {
        const auto b = a;
        auto c = b;
        // 3.
        EXPECT_TRUE(a == b);
        EXPECT_TRUE(b == c);
        EXPECT_TRUE(a == c);
      }
    }

    TYPED_TEST_P(nullable_const_pointer_concept, default_constructible)
    {
      using pointer =
        typename nullable_const_pointer_concept<TypeParam>::pointer;

      {
        // 1.
        pointer u;
      }

      {
        // 2.
        pointer u{};
      }

      {
        // 3.
        { pointer u = pointer(); }
        { pointer u = pointer{}; }
      }
    }

    TYPED_TEST_P(nullable_const_pointer_concept, copy_constructible)
    {
      using alloc_traits =
        typename nullable_const_pointer_concept<TypeParam>::alloc_traits;
      using pointer =
        typename nullable_const_pointer_concept<TypeParam>::pointer;

      {
        // 1.
        auto v = alloc_traits::allocate(*(this->_M_alloc), 1);
        pointer u = v;
      }

      {
        // 2.
        const auto v = alloc_traits::allocate(*(this->_M_alloc), 1);
        pointer u = pointer(v);
      }
    }

    TYPED_TEST_P(nullable_const_pointer_concept, copy_assignable)
    {
      using alloc_traits =
        typename nullable_const_pointer_concept<TypeParam>::alloc_traits;
      using pointer =
        typename nullable_const_pointer_concept<TypeParam>::pointer;

      {
        // 1.
        auto v = alloc_traits::allocate(*(this->_M_alloc), 1);
        pointer u;

        EXPECT_EQ(u = v, u);
      }
    }

    TYPED_TEST_P(nullable_const_pointer_concept, destructible)
    {
      using alloc_traits =
        typename nullable_const_pointer_concept<TypeParam>::alloc_traits;
      using pointer =
        typename nullable_const_pointer_concept<TypeParam>::pointer;

      {
        // 1.
        pointer v = alloc_traits::allocate(*(this->_M_alloc), 1);
        EXPECT_NO_THROW(v.~pointer());
      }
    }

    TYPED_TEST_P(nullable_const_pointer_concept, nullable)
    {
      using alloc_traits =
        typename nullable_const_pointer_concept<TypeParam>::alloc_traits;
      using pointer =
        typename nullable_const_pointer_concept<TypeParam>::pointer;

      {
        // 1.
        std::nullptr_t np;
        { pointer p(np); }
        { pointer p = np; }
      }

      {
        // 2.
        const std::nullptr_t np = nullptr;
        pointer p = pointer(np);
      }

      {
        // 3.
        const std::nullptr_t np = nullptr;
        pointer p;

        EXPECT_EQ(p = np, nullptr);
      }

      {
        // 4.
        auto p = alloc_traits::allocate(*(this->_M_alloc), 1);
        auto q = alloc_traits::allocate(*(this->_M_alloc), 1);
        EXPECT_TRUE(p != q);
      }

      {
        // 5.
        pointer p = alloc_traits::allocate(*(this->_M_alloc), 1);
        const std::nullptr_t np = nullptr;

        EXPECT_FALSE(p == np);
        EXPECT_FALSE(np == p);
      }

      {
        // 6.
        pointer p = alloc_traits::allocate(*(this->_M_alloc), 1);
        const std::nullptr_t np = nullptr;

        EXPECT_TRUE(p != np);
        EXPECT_TRUE(np != p);
      }
    }


    REGISTER_TYPED_TEST_CASE_P(nullable_const_pointer_concept,
                               equality_comparable,
                               default_constructible,
                               copy_constructible,
                               copy_assignable,
                               destructible,
                               nullable);

  } // ~namespace testing
} // ~namespace pgstl

#endif // PGSTL_TESTING_NULLABLE_CONST_POINTER_CONCEPT

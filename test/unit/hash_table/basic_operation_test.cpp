#include "hash_table.hpp"
#include <gtest/gtest.h>
#include <string>

namespace pgstl
{
	namespace test
	{
		TEST(basic_operation_test, simple)
		{
			pgstl::_hash_table<int, int> _M_hash_table(100);

			_M_hash_table.insert(11, 22);
			EXPECT_EQ(22, _M_hash_table.search(11));

			_M_hash_table.insert(12, 33);
			EXPECT_EQ(33, _M_hash_table.search(12));

			_M_hash_table.insert(13, 44);
			EXPECT_EQ(44, _M_hash_table.search(13));

			EXPECT_EQ(-1, _M_hash_table.search(666));
		}


		TEST(basic_operation_test, destructor)
		{
			pgstl::_hash_table<int, int> _M_hash_table(10);
			_M_hash_table.insert(10, 22);
			_M_hash_table.insert(11, 33);
			_M_hash_table.insert(12, 44);
			_M_hash_table.insert(13, 55);
			_M_hash_table.insert(14, 66);
		}

		TEST(basic_operation_test, erase) {
			pgstl::_hash_table<int, int> _M_hash_table(5);

			_M_hash_table.insert(11, 111);
			_M_hash_table.insert(22, 222);
			_M_hash_table.insert(33, 333);
			_M_hash_table.insert(44, 444);
			_M_hash_table.insert(55, 555);
			_M_hash_table.insert(66, 666);
			_M_hash_table.insert(77, 777);
			_M_hash_table.insert(88, 888);
			_M_hash_table.insert(99, 999);

			EXPECT_EQ(false, _M_hash_table.erase(123));

			EXPECT_EQ(true, _M_hash_table.erase(11));
			EXPECT_EQ(true, _M_hash_table.erase(22));
			EXPECT_EQ(true, _M_hash_table.erase(33));
			EXPECT_EQ(true, _M_hash_table.erase(44));
			EXPECT_EQ(true, _M_hash_table.erase(55));
			EXPECT_EQ(true, _M_hash_table.erase(66));
			EXPECT_EQ(true, _M_hash_table.erase(77));
			EXPECT_EQ(true, _M_hash_table.erase(88));
			EXPECT_EQ(true, _M_hash_table.erase(99));

			EXPECT_EQ(false, _M_hash_table.erase(123));
		}



		TEST(basic_operation_test, push_after_erase) {
			pgstl::_hash_table<int, int> _M_hash_table(10);

			_M_hash_table.insert(10, 22);
			EXPECT_EQ(22, _M_hash_table.search(10));

			_M_hash_table.erase(10);

			_M_hash_table.insert(10, 777);
			EXPECT_EQ(777, _M_hash_table.search(10));

			_M_hash_table.insert(15, 11111);
			EXPECT_EQ(11111, _M_hash_table.search(15));

			_M_hash_table.insert(15, 22222);
			EXPECT_EQ(22222, _M_hash_table.search(15));

			_M_hash_table.erase(15);

			EXPECT_EQ(-1, _M_hash_table.search(15));
		}


		TEST(basic_operation_test, char_key_type) {
			pgstl::_hash_table<std::string, int> _M_hash_table(100);

			std::string key = { 'k', 'e', 'y', '1', '\0' };

			_M_hash_table.insert(key, 22);
			EXPECT_EQ(22, _M_hash_table.search(key));

			key = "k";
			_M_hash_table.insert(key, 33);
			EXPECT_EQ(33, _M_hash_table.search(key));

			key = "key3";
			_M_hash_table.insert(key, 44);
			EXPECT_EQ(44, _M_hash_table.search(key));

			key = "(-.-)";
			_M_hash_table.insert(key, 55);
			EXPECT_EQ(55, _M_hash_table.search(key));

			key = "DiFeRenT";
			_M_hash_table.insert(key, 66);
			EXPECT_EQ(66, _M_hash_table.search(key));

			key = "dummyKey";
			EXPECT_EQ(-1, _M_hash_table.search(key));
		}

		TEST(basic_operation_test, duplicated_key) {
			pgstl::_hash_table<std::string, int> _M_char_key_hash_table(30);

			std::string key = { 'k', 'e', 'y', '1', '\0' };
			_M_char_key_hash_table.insert(key, 22);
			EXPECT_EQ(22, _M_char_key_hash_table.search(key));

			_M_char_key_hash_table.insert(key, 33);
			EXPECT_EQ(33, _M_char_key_hash_table.search(key));

			_M_char_key_hash_table.insert(key, 44);
			EXPECT_EQ(44, _M_char_key_hash_table.search(key));

			pgstl::_hash_table<int, int> _M_int_key_hash_table(30);

			_M_int_key_hash_table.insert(11, 123);
			EXPECT_EQ(123, _M_int_key_hash_table.search(11));

			_M_int_key_hash_table.insert(11, 456);
			EXPECT_EQ(456, _M_int_key_hash_table.search(11));

			_M_int_key_hash_table.insert(11, 789);
			EXPECT_EQ(789, _M_int_key_hash_table.search(11));

			_M_int_key_hash_table.insert_if_empty(11, 6464);
			EXPECT_EQ(789, _M_int_key_hash_table.search(11));

			_M_int_key_hash_table.insert_if_empty(4, 5555);
			EXPECT_EQ(5555, _M_int_key_hash_table.search(4));
		}

	} // namespace test
} // namespace pgstl
